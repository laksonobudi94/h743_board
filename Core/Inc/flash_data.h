#ifndef __FLASH_DATA
#define __FLASH_DATA

#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/api.h"
#include "cmd_process.h"

#if !defined(STM32H7)
#define ADDR_CFG 0x080C0000 // last sector of embedded flash memory
#define SECTOR_CFG 7
#else
//the size is 128K
#define ADDR_CFG 0x081E0000 // last sector of embedded flash memory
#define SECTOR_CFG 7
#define BANK_CFG 2
#endif

typedef struct
{
    u32_t ip_address_0;
    u32_t ip_address_1;
    u32_t netmask;
    u32_t gateway;
    s32_t listen_port[MAX_TCP_PORT];
} cfg_dat_t;

void addr_to_buff(u32_t addr, u8_t *buff);

static void default_config(cfg_dat_t *def_cfg);
static void erase_sector_cfg(void);
static u8_t save_cfg_from_mem(u8_t *data, u32_t len);
static u8_t read_cfg_from_mem(u8_t *data, u32_t len);

u8_t config_init(void);
cfg_dat_t *get_config_ptr(void);
u8_t save_config(void);

#endif /* __FLASH_DATA */
