#ifndef __PELCO_D
#define __PELCO_D

#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/api.h"

#define SYNC_BYTE 0xFF


typedef enum move_pan
{
    RIGHT_PAN = 0,
    LEFT_PAN,
    STOP_PAN
} pan_t;

typedef enum move_tilt
{
    UP_TILT = 0,
    DOWN_TILT,
    STOP_TILT
} tilt_t;

struct __attribute__((__packed__)) bit_cmd_1
{
    u8_t Focus_Near : 1;
    u8_t Iris_Open : 1;
    u8_t Iris_Close : 1;
    u8_t Camera_On_Off : 1;
    u8_t Auto_Manual_Scan : 1;
    u8_t Reserved : 2;
    u8_t Sense : 1;
};

struct __attribute__((__packed__)) bit_cmd_2
{
    u8_t Fixed_to_0 : 1;
    u8_t Pan_Right : 1;
    u8_t Pan_Left : 1;
    u8_t Tilt_Up : 1;
    u8_t Tilt_Down : 1;
    u8_t Zoom_Tele : 1;
    u8_t Zoom_Wide : 1;
    u8_t Focus_Far : 1;
};

union command_1
{
    struct bit_cmd_1 bit;
    u8_t byte;
};

union command_2
{
    struct bit_cmd_2 bit;
    u8_t byte;
};

typedef struct __attribute__((__packed__)) data_format
{
    u8_t Sync;
    u8_t Camera_Address;
    u8_t Command_1;
    u8_t Command_2;
    u8_t Data_1;
    u8_t Data_2;
    u8_t Checksum;
} pelco_d_data_t;

#endif /* __PELCO_D */
