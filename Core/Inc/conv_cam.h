#ifndef __CONVERENCE_CAM_H
#define __CONVERENCE_CAM_H

#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/api.h"
#include "lwip/tcp.h"

#define BIT_TX_COMPLETE (1 << 0)
#define BIT_RX_COMPLETE (1 << 1)

#define CHECK_BIT(x, pos) (x & (1UL << pos))

#define MAX_RX_SIZE_BUFF 64
#define RxBuf_SIZE 32
#define MainBuf_SIZE 64

// this is for testing
#define CONV_CAM_USE

#define P_val mask_4_bit(0)
#define Q_val mask_4_bit(0)
#define R_val mask_4_bit(0)
#define S_val mask_4_bit(0)
#define T_val mask_4_bit(0)
#define U_val mask_4_bit(0)
#define V_val mask_4_bit(0)
#define W_val mask_4_bit(0)

#define Y_val mask_4_bit(0)
#define Z_val mask_4_bit(0)

#define PP_val 0
#define VV_val 0
#define WW_val 0
#define ZZ_val 0

#define MINUS_170_DEGREE 0xF768
#define PLUS_170_DEGREE 0x0898

#define MINUS_30_DEGREE 0xFEB3
#define PLUS_90_DEGREE 0x04B0

typedef enum format_video
{
    R_1080P60 = 0,
    R_1080P50,
    R_1080I60,
    R_1080I50,
    R_1080P30,
    R_1080P25,
    R_720P60,
    R_720P50,
    R_720P30,
    R_720P25,
    R_1600_900_60_USB_OUTPUT,
    R_1440_900_60HZ_USB_OUTPUT,
    R_1366_768_60HZ_USB_OUTPUT,
    R_1280_800_60HZ_USB_OUTPUT,
    R_1024_768_60HZ_USB_OUTPUT,
    R_800_600_60HZ_USB_OUTPUT,
    R_800_600_30HZ_USB_OUTPUT,
    R_640_480_60HZ_USB_OUTPUT,
    R_640_480_30HZ_USB_OUTPUT
} format_video_t;

/*
    COMMAND SET
*/

static const u8_t AddressSet[] = {0x88, 0x30, 0x04, 0x01, 0xff};
static const u8_t IF_Clear[] = {0x88, 0x01, 0x00, 0x01, 0xff};
static const u8_t CommandCancel[] = {0x80 | ADDRESS_CAMERA, 0x21, 0xff};

static const u8_t CAM_Power_On[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x00, 0x02, 0xff};
static const u8_t CAM_Power_Off[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x00, 0x03, 0xff};

static const u8_t CAM_Zoom_Stop_Conv_Cam[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x07, 0x00, 0xff};
static const u8_t CAM_Zoom_Tele_Standard[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x07, 0x02, 0xff};
static const u8_t CAM_Zoom_Wide_Standard[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x07, 0x03, 0xff};

/*p = 0(low)~7(high)*/
static const u8_t CAM_Zoom_Tele_Variable[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x07, 0x20 | P_val, 0xff};
static const u8_t CAM_Zoom_Wide_Variable[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x07, 0x30 | P_val, 0xff};

/*pqrs: Zoom Position (0(wide) ~0x4000(tele))*/
static const u8_t CAM_Zoom_Direct[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x47, P_val, Q_val, R_val, S_val, 0xFF};

static const u8_t CAM_Focus_Stop_Conv_Cam[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x08, 0x00, 0xff};
static const u8_t CAM_Focus_Far_Standard[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x08, 0x02, 0xff};
static const u8_t CAM_Focus_Near_Standard[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x08, 0x03, 0xff};

/*pqrs: Zoom Position (0(wide) ~0x4000(tele))*/
static const u8_t CAM_Focus_Direct[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x48, P_val, Q_val, R_val, S_val, 0xFF};

static const u8_t One_Push_AF[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x18, 0x01, 0xff};

/*pqrs: Zoom Position (0(wide)~ 0x4000(tele)) tuvw: Focus Position*/
static const u8_t CAM_ZoomFocus_Direct[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x47, P_val, Q_val, R_val, S_val, T_val, U_val, V_val, W_val, 0xff};

static const u8_t CAM_WB_Auto[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x35, 0x00, 0xff};
static const u8_t CAM_WB_Indoor[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x35, 0x01, 0xff};
static const u8_t CAM_WB_Outdoor[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x35, 0x02, 0xff};
static const u8_t CAM_WB_OnePush[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x35, 0x03, 0xff};
static const u8_t CAM_WB_Manual[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x35, 0x05, 0xff};
static const u8_t CAM_WB_Outdoor_Auto[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x35, 0x06, 0xff};
static const u8_t CAM_WB_Sodium_Lamp_Auto[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x35, 0x07, 0xff};
static const u8_t CAM_WB_Sodium_Auto[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x35, 0x08, 0xff};

static const u8_t CAM_RGain_Reset[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x03, 0x00, 0xff}; /*Manual Control of R Gain*/
static const u8_t CAM_RGain_Up[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x03, 0x02, 0xff};
static const u8_t CAM_RGain_Down[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x03, 0x03, 0xff};

/*pq: R Gain (0~0xFF)*/
static const u8_t CAM_RGain_Direct[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x43, 0x00, 0x00, P_val, Q_val, 0xff};

static const u8_t CAM_Bgain_Reset[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x04, 0x00, 0xff}; /*Manual Control of B Gain*/

static const u8_t CAM_Bgain_Up[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x04, 0x02, 0xff};
static const u8_t CAM_Bgain_Down[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x04, 0x03, 0xff};

/*pq: R Gain (0~0xFF)*/
static const u8_t CAM_Bgain_Direct[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x44, 0x00, 0x00, P_val, Q_val, 0xff};

static const u8_t CAM_AE_Full[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x39, 0x00, 0xff};   /*Auto Automatic Exposure mode*/
static const u8_t CAM_AE_Manual[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x39, 0x03, 0xff}; /*Manual Control mode*/
static const u8_t CAM_AE_Bright[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x39, 0x0D, 0xff}; /*Bright mode(Manual control)*/

static const u8_t CAM_Shutter_Reset[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x0A, 0x00, 0xff}; /*Shutter Setting*/
static const u8_t CAM_Shutter_Up[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x0A, 0x02, 0xff};
static const u8_t CAM_Shutter_Down[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x0A, 0x03, 0xff};

/*pq: Shutter Position (0~0x15)*/
static const u8_t CAM_Shutter[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x4A, 0x00, 0x00, P_val, Q_val, 0xff};

static const u8_t CAM_Iris_Reset[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x0B, 0x00, 0xff};
static const u8_t CAM_Iris_Up[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x0B, 0x00, 0xff};
static const u8_t CAM_Iris_Down[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x0B, 0x00, 0xff};

/*pq: Iris Position (0~ 0x11)*/
static const u8_t CAM_Iris_Direct[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x4B, 0x00, 0x00, P_val, Q_val, 0xff};

static const u8_t CAM_Bright_Reset[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x0D, 0x00, 0xff}; /*Bright Setting*/
static const u8_t CAM_Bright_Up[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x0D, 0x02, 0xff};
static const u8_t CAM_Bright_Down[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x0D, 0x03, 0xff};

/*pq: Bright l Positon ()*/
static const u8_t CAM_Bright_Direct[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x4D, 0x00, 0x00, P_val, Q_val, 0xff};

static const u8_t CAM_ExpComp_On[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x3E, 0x02, 0xff}; /*Exposure Compensation ON/OFF*/
static const u8_t CAM_ExpComp_Off[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x3E, 0x03, 0xff};
static const u8_t CAM_ExpComp_Reset[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x0E, 0x00, 0xff}; /*Exposure Compensation Amount Setting*/
static const u8_t CAM_ExpComp_Up[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x0E, 0x02, 0xff};
static const u8_t CAM_ExpComp_Down[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x0E, 0x03, 0xff};

/*pq: ExpComp Position (0~0x0E)*/
static const u8_t CAM_ExpComp_Direct[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x4E, 0x00, 0x00, P_val, Q_val, 0xff};

static const u8_t CAM_BackLight_On[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x33, 0x02, 0xff};
static const u8_t CAM_BackLight_Off[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x33, 0x03, 0xff};

static const u8_t CAM_Aperture_Reset[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x02, 0x00, 0xff}; /*Aperture Control*/
static const u8_t CAM_Aperture_Up[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x02, 0x02, 0xff};
static const u8_t CAM_Aperture_Down[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x02, 0x03, 0xff};

/*pq: Aperture Gain (0~0x04)*/
static const u8_t CAM_Aperture_Direct[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x42, 0x00, 0x00, P_val, Q_val, 0xff};

/*p: Memory Number(=0 to 127)*/
static const u8_t CAM_Memory_Reset[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x3F, 0x00, P_val, 0xff};
/*Corresponds to 0 to 9 on the Remote Commander*/
static const u8_t CAM_Memory_Set[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x3F, 0x01, P_val, 0xff};
/*p: Memory Number(=0 to 127)*/
static const u8_t CAM_Memory_Recall[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x3F, 0x02, P_val, 0xff};

/*Image Flip Horizontal ON/OFF*/
static const u8_t CAM_LR_Reverse_On[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x61, 0x02, 0xff};
static const u8_t CAM_LR_Reverse_Off[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x61, 0x03, 0xff};

/*Image Flip Vertical ON/OFF*/
static const u8_t CAM_PictureFlip_On[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x66, 0x02, 0xff};
static const u8_t CAM_PictureFlip_Off[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x66, 0x03, 0xff};

static const u8_t CAM_MountMode_On[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0xA4, 0x02, 0xff};
static const u8_t CAM_MountMode_Off[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0xA4, 0x03, 0xff};

/*(0~0x0E)*/
static const u8_t CAM_ColorGain_Direct[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x49, 0x00, 0x00, 0x00, P_val, 0xff};
static const u8_t CAM_2D_Noise_reduction_Direct[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x53, P_val, 0xff};
static const u8_t CAM_3D_Noise_reduction_Direct[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x54, P_val, 0xff};

static const u8_t FLICK_50HZ[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x23, 0x01, 0xff};
static const u8_t FLICK_60HZ[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x23, 0x02, 0xff};

static const u8_t Freeze_On_Immediately[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x62, 0x02, 0xff};
static const u8_t Freeze_Off_Immediately[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x62, 0x02, 0xff};
static const u8_t Freeze_On_When_Running_Preset[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x62, 0x02, 0xff};
static const u8_t Freeze_Off_When_Running_Preset[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x62, 0x02, 0xff};

/*pp: 0~18*/
static const u8_t Video_format[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x35, 0x00, PP_val, 0xff};

/* pqrs: Camera ID (=0000 to FFFF)*/
static const u8_t CAM_IDWrite[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x22, P_val, Q_val, R_val, S_val, 0xff};

/*the menu*/
static const u8_t SYS_Menu_Turn_on[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x06, 0x02, 0xFF};
static const u8_t SYS_Menu_Turn_off[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x06, 0x03, 0xFF};
static const u8_t SYS_Menu_Menu_step_back[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x06, 0x10, 0xFF};
static const u8_t SYS_Menu_Menu_ok[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x7E, 0x01, 0x02, 0x00, 0x01, 0xff};

/*Receive IR (remote commander) CODE from VISCA communication ON/OFF*/
static const u8_t IR_Transfer_On[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x1A, 0x02, 0xff};
static const u8_t IR_Transfer_Off[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x1A, 0x03, 0xff};

/*IR (remote commander) receive ON/OFF*/
static const u8_t IR_Receive_On[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x08, 0x02, 0xff};
static const u8_t IR_Receive_Off[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x08, 0x03, 0xff};
static const u8_t IR_Receive_On_Off[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x10, 0x03, 0xff};

/*IR (remote commander) receive message via the VISCA communication ON/OFF*/
static const u8_t IR_ReceiveReturn_On[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x7D, 0x01, 0x03, 0x00, 0x00, 0xff};
static const u8_t IR_ReceiveReturn_Off[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x7D, 0x01, 0x13, 0x00, 0x00, 0xff};

/*VV: Pan speed 0x01 (low speed) to 0x18 (high speed)*/
/*WW: Tilt speed 0x01 (low speed) to 0x14 (high speed)*/
/*VV: Pan speed 0x01 (low speed) to 0x18 (high speed)*/
/*YYYY: Pan Position (TBD)*/
static const u8_t Pan_tiltDrive_Up[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x01, VV_val, WW_val, 0x03, 0x01, 0xff};
static const u8_t Pan_tiltDrive_Down[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x01, VV_val, WW_val, 0x03, 0x02, 0xff};
static const u8_t Pan_tiltDrive_Left[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x01, VV_val, WW_val, 0x01, 0x03, 0xff};
static const u8_t Pan_tiltDrive_Right[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x01, VV_val, WW_val, 0x02, 0x03, 0xff};
static const u8_t Pan_tiltDrive_UpLeft[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x01, VV_val, WW_val, 0x01, 0x01, 0xff};
static const u8_t Pan_tiltDrive_UpRight[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x01, VV_val, WW_val, 0x02, 0x01, 0xff};
static const u8_t Pan_tiltDrive_DownLeft[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x01, VV_val, WW_val, 0x01, 0x02, 0xff};
static const u8_t Pan_tiltDrive_DownRight[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x01, VV_val, WW_val, 0x02, 0x02, 0xff};
static const u8_t Pan_tiltDrive_Stop[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x01, VV_val, WW_val, 0x03, 0x03, 0xff};
static const u8_t Pan_tiltDrive_AbsolutePosition[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x02, VV_val, WW_val, Y_val, Y_val, Y_val, Y_val, Z_val, Z_val, Z_val, Z_val, 0xff};
static const u8_t Pan_tiltDrive_RelativePosition[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x03, VV_val, WW_val, Y_val, Y_val, Y_val, Y_val, Z_val, Z_val, Z_val, Z_val, 0xff};
static const u8_t Pan_tiltDrive_Home[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x04, 0xff};
static const u8_t Pan_tiltDrive_Reset[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x05, 0xff};

#define W_val mask_4_bit(0)

/*W:1 UpRight 0:DownLeft*/
/*YYYY: Pan Limit Position(TBD)*/
/*ZZZZ: Tilt Limit Position(TBD)*/
static const u8_t Pan_tiltLimitSet_Set[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x07, 0x00, W_val, Y_val, Y_val, Y_val, Y_val, Z_val, Z_val, Z_val, Z_val, 0xff};
static const u8_t Pan_tiltLimitSet_Clear[] = {0x80 | ADDRESS_CAMERA, 0x01, 0x06, 0x07, 0x01, W_val, 0x07, 0x0F, 0x0F, 0x0F, 0x07, 0x0F, 0x0F, 0x0F, 0xff};

/*
    Inquiry commands
*/

static const u8_t CAM_PowerInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x00, 0xff};
static const u8_t CAM_ZoomPosInq_Conv_Cam[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x47, 0xff};
static const u8_t CAM_FocusModeInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x38, 0xff};
static const u8_t CAM_FocusPosInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x48, 0xff};
static const u8_t CAM_WBModeInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x35, 0xff};
static const u8_t CAM_RGainInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x43, 0xff};
static const u8_t CAM_BGainInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x44, 0xff};
static const u8_t CAM_AEModeInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x39, 0xff};
static const u8_t CAM_ShutterPosInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x4a, 0xff};
static const u8_t CAM_IrisPosInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x4b, 0xff};
static const u8_t CAM_GainPosiInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x4c, 0xff};
static const u8_t BrightPosiInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x4d, 0xff};
static const u8_t CAM_ExpCompModeInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x3e, 0xff};
static const u8_t CAM_ExpCompPosInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x4e, 0xff};
static const u8_t CAM_ApertureInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x42, 0xff};
static const u8_t CAM_MemoryInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x3f, 0xff};
static const u8_t SYS_MenuModeInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x06, 0x06, 0xff};
static const u8_t CAM_LR_ReverseInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x61, 0xff};
static const u8_t CAM_PictureFlipInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x66, 0xff};
static const u8_t CAM_IDInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x22, 0xff};
static const u8_t CAM_VersionInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x00, 0x02, 0xff};
static const u8_t VideoSystemInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x06, 0x23, 0xff};
static const u8_t IR_Transfer[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x06, 0x1a, 0xff};
static const u8_t IR_Receive[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x06, 0x08, 0xff};

// IR_ReceiveReturn

static const u8_t Pan_tiltMaxSpeedInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x06, 0x11, 0xff};
static const u8_t Pan_tiltPosInq[] = {0x80 | ADDRESS_CAMERA, 0x09, 0x06, 0x12, 0xff};

/*
    Return Packet
*/

/* CAM_PowerInq => On / Off (Standby) */
/* CAM_FocusModeInq => Auto Focus / Manual Focus */
/* CAM_ExpCompModeInq => On / Off (Standby) */
/* SYS_MenuModeInq => On / Off (Standby) */
/* CAM_LR_ReverseInq => On / Off (Standby) */
/* CAM_PictureFlipInq => On / Off (Standby) */
/* IR_Transfer => On / Off (Standby) */
/* IR_Receive => On / Off (Standby) */
static const u8_t On[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x02, 0xff};
static const u8_t Off[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x03, 0xff};

/* CAM_FocusPosInq => pqrs: Focus Position */
/* CAM_IDInq => pqrs: Camera ID */
static const u8_t PQRS_Position[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, P_val, Q_val, R_val, S_val, 0xff};

/* CAM_WBModeInq */
static const u8_t Auto[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x00, 0xff};
static const u8_t Indoor_mode[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x01, 0xff};
static const u8_t Outdoor_mode[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x02, 0xff};
static const u8_t OnePush_mode[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x03, 0xff};
static const u8_t ATW[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x04, 0xff};
static const u8_t Manual[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x05, 0xff};

/* CAM_RGainInq => pq: R Gain */
/* CAM_BGainInq => pq: B Gain */
/* CAM_ShutterPosInq => pq: Shutter Position */
/* CAM_IrisPosInq => pq: Iris Position */
/* CAM_GainPosiInq => pq: Gain Position */
/* BrightPosiInq => pq: Bright Position */
/* CAM_ExpCompPosInq => pq: ExpComp Position */
/* CAM_ApertureInq => pq: Aperture Gain */
static const u8_t PQ_Position[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x00, 0x00, P_val, Q_val, 0xff};

/* CAM_AEModeInq */
static const u8_t Full_Auto[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x00, 0xff};
static const u8_t Manual_AE[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x03, 0xff};
static const u8_t Shutter_priority[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x0A, 0xff};
static const u8_t Iris_priority[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x0B, 0xff};
static const u8_t Bright[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, 0x0F, 0xff};

/* CAM_MemoryInq => pp: Memory number last operated. */
/* CAM_VersionInq => pp: 0~18 Video format */
static const u8_t PP_Position[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, PP_val, 0xff};

/* IR_ReceiveReturn */
static const u8_t Power_ON_OFF[] = {0x80 | (ADDRESS_CAMERA << 4), 0x07, 0x7d, 0x01, 0x04, 0x00, 0xff};
static const u8_t Zoom_tele_wide[] = {0x80 | (ADDRESS_CAMERA << 4), 0x07, 0x7d, 0x01, 0x04, 0x07, 0xff};
static const u8_t AF_On_Off[] = {0x80 | (ADDRESS_CAMERA << 4), 0x07, 0x7d, 0x01, 0x04, 0x38, 0xff};
static const u8_t CAM_Backlight[] = {0x80 | (ADDRESS_CAMERA << 4), 0x07, 0x7d, 0x01, 0x04, 0x33, 0xff};
static const u8_t CAM_Memory[] = {0x80 | (ADDRESS_CAMERA << 4), 0x07, 0x7d, 0x01, 0x04, 0x3f, 0xff};
static const u8_t Pan_tiltDrive[] = {0x80 | (ADDRESS_CAMERA << 4), 0x07, 0x7d, 0x01, 0x04, 0x01, 0xff};

static const u8_t response_Pan_tiltMaxSpeedInq[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, WW_val, ZZ_val, 0xff};
static const u8_t response_Pan_tiltPosInq[] = {0x80 | (ADDRESS_CAMERA << 4), 0x50, W_val, W_val, W_val, W_val, Z_val, Z_val, Z_val, Z_val, 0xff};

// Returned when the command format is different or when a command with illegal command parameters is accepted.
static const u8_t Syntax_Error[] = {0x80 | (ADDRESS_CAMERA << 4), 0x60, 0x02, 0xff};
// Returned when a command cannot be executed due to current conditions. For example, when commands controlling the focus manually are received during auto focus.
static const u8_t Command_Not_Executable[] = {0x80 | (ADDRESS_CAMERA << 4), 0x61, 0x41, 0xff};
// Returned when the command is accepted (Ack)
static const u8_t reply_ACK[] = {0x80 | (ADDRESS_CAMERA << 4), 0x41, 0xFF};
// Returned when the command has been executed (completion).
static const u8_t Completion[] = {0x80 | (ADDRESS_CAMERA << 4), 0x51, 0xFF};

typedef enum
{
    Pan_Angle_min_170 = 0xF670,
    Pan_Angle_min_135 = 0xF868,
    Pan_Angle_min_90 = 0xFAF0,
    Pan_Angle_min_45 = 0xFD78,
    Pan_Angle_0 = 0x0000,
    Pan_Angle_45 = 0x0288,
    Pan_Angle_90 = 0x0510,
    Pan_Angle_135 = 0x0798,
    Pan_Angle_170 = 0x0990
} Pan_Angle_t;

typedef enum
{
    Pan_Tilt_min_30 = 0xFE50,
    Pan_Tilt_0 = 0x0000,
    Pan_Tilt_30 = 0x01B0,
    Pan_Tilt_60 = 0x0360,
    Pan_Tilt_90 = 0x510
} Tilt_Angle_t;

#define MIN_LOW_SPEED 0x01
#define MAX_HIGH_SPEED 0x18

typedef enum
{
    Up = 0,
    Down,
    Left,
    Right,
    Upleft,
    Upright,
    Downleft,
    Downright,
    Stop,
    AbsolutePosition,
    RelativePosition,
    Home,
    Reset,
} Pan_tiltDrive_t;

s16_t map_s32_to_s16(s32_t x, s32_t in_min, s32_t in_max, s16_t out_min, s16_t out_max);
s16_t map_s16(s16_t x, s16_t in_min, s16_t in_max, s16_t out_min, s16_t out_max);
s32_t map_s16_to_s32(s16_t x, s16_t in_min, s16_t in_max, s32_t out_min, s32_t out_max);

size_t CMD_Pan_Tilt_Drive(Pan_tiltDrive_t move_type, u8_t *buff, u8_t Pan_speed, u8_t Tilt_speed);
u8_t CMD_Set_PT_Position(Pan_tiltDrive_t move_type, u16_t Pan_Position, u16_t Tilt_Position, u8_t Pan_speed, u8_t Tilt_speed, u8_t *buff);

u16_t CMD_Get_PT_Position(u8_t *buff);
u8_t Parse_Get_PT_Position(u8_t *buff, u16_t *pan_visca_value, u16_t *tilt_visca_value);
u8_t Parse_Get_PT_Speed(u8_t *Pan_speed, u8_t *Tilt_speed);
err_t check_position(s16_t velo_val,
                     u16_t limit,
                     u16_t pos_now,
                     s16_t *temp_velo,
                     u16_t *set_pos,
                     s16_t *move_elevate);
u8_t set_home_PT(void);
u8_t get_encoder_pos(u16_t *azimuth_encoder_pos,
                     u16_t *elevace_encoder_pos,
                     s16_t *pan_angle,
                     s16_t *tilt_angle);
#endif /*__CONVERENCE_CAM_H*/
