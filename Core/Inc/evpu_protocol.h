#ifndef __EVPU_PROTOCOL
#define __EVPU_PROTOCOL

#include "lwip/opt.h"
#include "stdio.h"
#include "string.h"

#include "lwip.h"
#include "lwip/init.h"
#include "lwip/netif.h"
#include "lwip/api.h"

#if defined(STM32H7)
#include "FreeRTOS.h"
#include "semphr.h"
#include "timers.h"
#endif

#define TIMEOUT_TICK 1000

#define SEM_TIMEOUT 1000 / portTICK_PERIOD_MS

#define EVPU_PROTOCOL_PORT 10000

#define MY_PREFIX 0x0120
#define LEN_CMD_1 5
#define LEN_CMD_0 6
#define MAX_MULTIPLE_SW 5

#define MAINBOARD_SER "s0"
#define DAY_CAM_SER "s1"
#define IR_CAM_SER "s2"
#define LRF_0_SER "s3"
#define LRF_1_SER "s4"
#define GPS "s5"

#define AZIMUTH_SYNC_CMD "a syn"
#define ELEVATE_SYNC_CMD "e syn"
#define AZIMUTH_VEL_LIMIT_CMD "a lim"
#define ELEVATE_VEL_LIMIT_CMD "e lim"
#define AZIMUTH_VELO_CMD "a vel"
#define ELEVATE_VELO_CMD "e vel"
#define AZIMUTH_POSITION_CMD "a pos"
#define ELEVATE_POSITION_CMD "e pos"

#define REQ_ELEVATE_MIN_DEG "e gmin"
#define REQ_ELEVATE_MAX_DEG "e gmax"
#define REQ_AZIMUTH_SYNC_STAT "a gsyn"
#define REQ_ELEVATE_SYNC_STAT "e gsyn"
#define CFG_SEND_PERIOD_PT_POS "period"
#define SET_SWITCH "o"

#define SEND_PERIOD_PT_POS "per"

#define SERIAL_CFG "mode"
#define CMD_DAT "data"
#define AZIMUTH_VEL "a vel"
#define ELEVATE_VEL "e vel"

#define END_MSG_PC 0x0d0a  //"\n\r" CRLF
#define END_MSG_MCU 0x0a0d //"\r\n" LFCR
#define MAX_LEN_SEND 100

typedef enum visca
{
    CAM_Zoom_Tele = 0, /*Zoom +*/
    CAM_Zoom_Stop = 1,
    CAM_ZoomPosInq = 2,
    CAM_Zoom_Wide = 3, /*Zoom -*/
    CAM_Focus_Auto_Focus = 4,
    CAM_Focus_Manual_Focus = 5,
    CAM_Focus_Near = 6, /*Foc -*/
    CAM_Focus_Stop = 7,
    CAM_Focus_Far = 8, /*Foc +*/
    ACK,
    COMPLETE_CMD_EXECUTED,
    SIZE_ENUM_CCD
} visca_t;

typedef enum ir_protocol
{
    ZoomPlus = 0, /*Zoom +*/
    ZoomStopFocusStop,
    ZoomStopEnd,
    ZoomMinus, /*Zoom -*/
    AutoFocus, /*Toogled*/
    FocusMinus,
    FocusPlus,
    SIZE_ENUM_IR
} ir_protocol_t;

typedef enum
{
    NO_SET_POS_AZIMUTH = 0,
    RIGHT_MOVE_AZIMUTH,
    LEFT_MOVE_AZIMUTH
} azimuth_arrow_t;
typedef enum
{
    NO_SET_POS_ELEVATE = 0,
    UP_MOVE_ELEVATE,
    DOWN_MOVE_ELEVATE
} elevate_arrow_t;

typedef struct cfg_mainboard
{
    u16_t max_elev_deg;
    u16_t min_elev_deg;
    u8_t a_sync_stat;
    u8_t e_sync_stat;
    u32_t period_send_p_t;
    u16_t max_azimuth_vel_limit;
    u16_t max_elevate_vel_limit;
} cfg_t;

typedef struct periodic_dat
{
    u16_t set_azimuth_pos;
    u16_t set_elevate_pos;
    u16_t azimuth_pos_now;
    u16_t elevace_pos_now;
    s16_t move_azimut;
    s16_t move_elevate;
    u8_t vsupply;
    u64_t status_alarm;
} periodic_dat_t;

typedef enum serial
{
    SERIAL0 = 0,
    SERIAL1,
    SERIAL2,
    SERIAL3,
    SERIAL4,
    SERIAL5,
    UNKNOWN,
} serial_t;

typedef enum
{
    INIT_EVPU_SUCCESS = 0,
    CREATE_EVENT_485_ERR = 1,
    CREATE_SEM_ETH_ERR = 2,
    CREATE_SEM_UART_ERR = 3,
    CREATE_TIMER_ERR = 4,
    CREATE_THD_RX_UART_ERR = 5,
    CREATE_THD_RX_CAN_ERR = 6,
    CREATE_SET_HOME_PT_ERR = 7,
    GET_POS_PT_ERR = 8,
    GET_OBJ_POS_ERR = 9,
    GET_ENCODER_PT_ERR = 10
} init_evpu_err_t;

#define EMPTY 0x00
static const u8_t visca_protocol_list[SIZE_ENUM_CCD][7] = {{0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x07, 0x24, 0xff},  /*0 0x24 => 4 (Variable)  p=0 (Low) to 7 (High) */
                                                           {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x07, 0x00, 0xff},  /*1*/
                                                           {0x80 | ADDRESS_CAMERA, 0x09, 0x04, 0x47, 0xff, EMPTY}, /*2*/
                                                           {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x07, 0x34, 0xff},  /*3 0x34 => 4 (Variable)  p=0 (Low) to 7 (High) */
                                                           {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x38, 0x02, 0xff},  /*4*/
                                                           {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x38, 0x03, 0xff},  /*5*/
                                                           {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x08, 0x31, 0xff},  /*6 0x31 => 1 (Variable)  p=0 (Low) to 7 (High) */
                                                           {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x08, 0x00, 0xff},  /*7*/
                                                           {0x80 | ADDRESS_CAMERA, 0x01, 0x04, 0x08, 0x21, 0xff},  /*8 0x21 => 1 (Variable)  p=0 (Low) to 7 (High) */
                                                           {0x90, 0x41, 0xff, EMPTY, EMPTY, EMPTY},
                                                           {0x90, 0x51, 0xff, EMPTY, EMPTY, EMPTY}};

static char *name_enum_ccd[] = {"CAM_Zoom_Tele",
                                "CAM_Zoom_Stop",
                                "CAM_ZoomPosInq",
                                "CAM_Zoom_Wide",
                                "CAM_Focus_Auto_Focus",
                                "CAM_Focus_Manual_Focus",
                                "CAM_Focus_Near",
                                "CAM_Focus_Stop",
                                "CAM_Focus_Far"};

static char *name_enum_ir[] = {"ZoomPlus",
                               "ZoomStopFocusStop",
                               "ZoomStopEnd",
                               "ZoomMinus",
                               "AutoFocus",
                               "FocusMinus",
                               "FocusPlus"};

/* last byte is checksum, exclude AutoFocus & FocusStop*/
static const u8_t ir_protocol_list[SIZE_ENUM_IR][9] = {{0x24, 0x03, 0x05, 0x27, 0x00, 0x7d, 0x00, 0x01, 0x79},
                                                       {0x24, 0x03, 0x05, 0x7d, 0x00, 0x7d, 0x00, 0x01, 0x23},
                                                       {0x24, 0x04, 0x00, 0x20, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
                                                       {0x24, 0x03, 0x05, 0x00, 0x00, 0x7d, 0x00, 0x01, 0x5e},
                                                       {0x41, 0x46, 0x55, 0x0d, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
                                                       {0x24, 0x03, 0x05, 0x7d, 0x00, 0x00, 0x00, 0x01, 0x5e},
                                                       {0x24, 0x03, 0x05, 0x7d, 0x00, 0x27, 0x10, 0x01, 0x69}};

u8_t switch_video(u8_t vid_num, u8_t mode);
cfg_t *get_cfg_mainboard(void);
u8_t mainboard_process(u8_t *buff, u16_t *len, u8_t *need_reply);
u8_t evpu_parse_buff(u8_t *buff, u16_t *len, u8_t *need_reply);
u8_t send_serial(serial_t serID, u8_t *buff, u16_t len);
init_evpu_err_t init_evpu(void);
u8_t clear_obj_evpu_tcp(void);
void set_obj_evpu_tcp(struct netconn *tcp);
SemaphoreHandle_t get_sem_obj_ETH(void);

void init_usart_rx_thread(void *arg);
void usart_rx_IT_thread(void *arg);

// static u8_t Move_PanTilt(periodic_dat_t *pos);
static u8_t Set_Pos_PanTilt(periodic_dat_t *pos);
u8_t CMD_TransReceive(u8_t *buff, u16_t *len, TickType_t tick_timeout);
err_t write_netconn_sem(struct netconn *objTcp, u8_t *buff, u16_t len);
#endif /* _EVPU_PROTOCOL */
