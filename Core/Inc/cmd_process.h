#ifndef __CMD_PROCESS
#define __CMD_PROCESS

#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/api.h"
#include "lwip/tcp.h"

#if defined(STM32H7)
#include "FreeRTOS.h"
#include "queue.h"
#endif

#define CHECK_BIT(x, pos) (x & (1UL << pos))

#define SIZE_BUFFER 1024

#define MAX_TCP_CLIENT 2
#define MAX_TCP_PORT 2

#define QRECV_TIMEOUT_MS 1000 / portTICK_PERIOD_MS
#define QSEND_WAIT_MS 100 / portTICK_PERIOD_MS
#define IMMEDIATELY 0

#define qCmdProcess_LEN 100
#define REPLY_qCmdProcess_LEN 100

#define STACK_SIZE_CMD configMINIMAL_STACK_SIZE * 15
#define CMD_PROCESS_THREAD_PRIO (tskIDLE_PRIORITY + 3)

// command protocol for testing
#define FRAME_HEADER 0xEE
#define GET_COMMAND 0x0
#define SET_COMMAND 0x1

#define IP_ADDR 0x14
#define GATEWAY_ADDR 0x64
#define NETMASK_ADDR 0x74

#define LISTEN_PORT_1 0x81
#define LISTEN_PORT_2 0x82
#define LISTEN_PORT_3 0x83
#define LISTEN_PORT_4 0x84
#define LISTEN_PORT_5 0x85

#define DEFAUL_PORT_LISTEN_1 501
#define DEFAUL_PORT_LISTEN_2 10001
#define DEFAUL_PORT_LISTEN_3 50001
#define DEFAUL_PORT_LISTEN_4 -1 // mean disable
#define DEFAUL_PORT_LISTEN_5 -1 // mean disable

typedef enum
{
    CMD_THD = 0,
    TCP_CLIENT_THD_1,
    TCP_CLIENT_THD_2,
    TCP_CLIENT_THD_3,
    TCP_CLIENT_THD_4,
    TCP_CLIENT_THD_5,
} thread_id_t;

typedef struct
{
    u32_t payload_0;
    u32_t payload_1;
    u32_t payload_2;
    u32_t payload_3;
    u32_t payload_4;
} Message;

typedef struct id_client
{
    u8_t num_con;
    u32_t addr;
} id_client_t;

u8_t cmd_process_init(void);

u8_t clear_identity_client(u32_t addr);
u8_t set_identity_client(u32_t addr, u8_t idx);
u8_t get_idx_queue_reply(u32_t addr, u8_t *val);

err_enum_t DoCmdProcess(QueueHandle_t thd_queue,
                        u32_t source_port,
                        u8_t *buf,
                        u16_t *length,
                        u8_t *need_reply);
void cmd_process_thread(void *arg);
QueueHandle_t create_queue_reply(u8_t num_queue);
QueueHandle_t get_queue_reply(u8_t num_queue);
void free_queue(QueueHandle_t queue_free);

#endif /* _CMD_PROCESS */
