#ifndef __TCPECHO
#define __TCPECHO

#include "lwip/sys.h"
#include "lwip/api.h"

typedef enum
{
    TCP_SERVER_INIT_SUCCESS = 0,
    GET_THD_ID_ERR = 1,
    CREATE_TCP_CLIENT_EVENT_ERR = 2,
    CREATE_LPORT_EVENT_ERR = 3,
    INIT_EVPU_ERR = 4,
    CMD_PROCESS_INIT_ERR = 5,
    CREATE_SPV_THD_ERR = 6,
    CREATE_SERVER_OBJ_ERR = 7,
    CREATE_LPORT_THD_ERR = 8,
    LPORT_EVENT_BIT_ERR = 9,
} tcp_server_init_err_t;

#define TCP_SVC_THREAD_PRIO (tskIDLE_PRIORITY + 2)
#define TCP_SERVER_THREAD_PRIO (tskIDLE_PRIORITY + 4)
#define TCP_CLIENT_SVC_THREAD_PRIO (TCP_SERVER_THREAD_PRIO + MAX_TCP_PORT)

tcp_server_init_err_t tcp_server_init(void);
struct netconn *get_object_lwip(u8_t idx);

static void tcp_client_thread(void *arg);

#endif /* __TCPECHO */
