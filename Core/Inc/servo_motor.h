#ifndef __CAN_DRIVER
#define __CAN_DRIVER

#include "lwip/opt.h"
#include "stdio.h"
#include "string.h"

#include "lwip.h"
#include "lwip/init.h"
#include "lwip/netif.h"
#include "lwip/api.h"

#if defined(STM32H7)
#include "FreeRTOS.h"
#include "semphr.h"
#include "timers.h"
#endif

#include "stm32h7xx_hal_fdcan.h"

#define SERVO_DRIVE_ID 0

#define MAX_FREQ 50
#define MIN_GAIN 1
#define MAX_GAIN 127
#define MIN_RANGE_POS -134217728
#define MAX_RANGE_POS 134217727
#define MIN_RANGE_SPEED -20000
#define MAX_RANGE_SPEED 20000
#define MIN_RANGE_WAVE 0
#define MAX_RANGE_WAVE 4096
#define MIN_GEAR_NUMBER 500
#define MAX_GEAR_NUMBER 16384
#define MIN_LINE_NUMBER 500
#define MAX_LINE_NUMBER 4095

#define MASK_5_BYTE 0x1F

/*
11-bit Identifier Consists both Drive ID and Command Function Code:
b4~b0 = 5-bit Function Code (FC)
b5~b10 = Drive ID 0~64
11-bit Identifier Structure:
*/
typedef enum
{
    FC_bit_0 = 0,
    FC_bit_1,
    FC_bit_2,
    FC_bit_3,
    FC_bit_4,
    Drive_ID_bit_0,
    Drive_ID_bit_1,
    Drive_ID_bit_2,
    Drive_ID_bit_3,
    Drive_ID_bit_4,
    Drive_ID_bit_5,
} identifier_t;

/*
RTR Field:
Remote transmission request (RTR):
The DYN servo drive CAN protocol does not support Remote Frame functionality.
*/

typedef enum
{
    Set_Origin = 0x00,           /*Set absolute zero origin*/
    Go_Absolute_Pos_PTP = 0x01,  /*Point to Point Absolute Move*/
    Make_LinearLine = 0x02,      /*Linear Interpolated Move*/
    Go_Relative_Pos_PTP = 0x03,  /*Point to Point Relative Move*/
    Make_CircularArc = 0x04,     /*Circular Interpolated Move*/
    Assign_Drive_ID = 0x05,      /*Assign Drive ID*/
    Read_Drive_ID = 0x06,        /*Read Drive ID*/
    Set_Drive_Config = 0x07,     /*Set Drive configuration*/
    Read_Drive_Config = 0x08,    /*Read Drive configuration*/
    Read_Drive_Status = 0x09,    /*Read Drive status*/
    Turn_ConstSpeed = 0x0a,      /*Turn constant speed command*/
    Square_Wave = 0x0b,          /*Square wave motion command*/
    Sin_Wave = 0x0c,             /*Sine wave motion command*/
    SS_Frequency = 0x0d,         /*Square/Sine wave motion frequency*/
    General_Read = 0x0e,         /*Read motor position or torque*/
    ForMotorDefine = 0x0f,       /*Internal Function - Not user accessible*/
    Set_Param_Group1 = 0x10,     /*Set_MainGain, Speed Gain, Int Gain, Trq Cons*/
    Set_Param_Group2 = 0x11,     /*Set High Speed, High Accel*/
    Set_Param_Group3 = 0x12,     /*Set On Pos Range, GEAR_NUM, LINE_NUM*/
    Read_Param_Group1 = 0x13,    /*Read Main Gain, Speed Gain, Int Gain, Trq Cons*/
    Read_Param_Group2 = 0x14,    /*Read High Speed, High Accel*/
    Read_Param_Group3 = 0x15,    /*Read On Pos Range, GEAR_NUM, LINE_NUM*/
    Go_Absolute_Pos_Sync = 0x16, /*Synchronized ABS move command*/
    Go_Relative_Pos_Sync = 0x17, /*Synchronized REL move command*/
    Sync_Trigger = 0x18,         /*Synchronized move command trigger*/
    Servo_Disable = 0x19,        /*Servo Enable/Disable Input*/
    Read_Motor_Speed = 0x1A,     /*Read motor speed*/
    TBD = 0x1B,                  /*TBD*/
    Go_Relative_Pos_PD = 0x1C,   /*Position Direct Relative Move*/
    Drive_Error = 0x1D,          /*CAN error message output*/
    Echo_Function = 0x1E,        /*4 byte data echo*/
    Diagnostic_counter = 0x1F    /*Diagnostic counter output*/
} FC_5_bit_t;

typedef enum
{
    DEG_45 = 1024,
    DEG_90 = 2048,
    DEG_180 = 4096,
} amplitude_enum_t;

typedef enum
{
    Absolute_Pos_Sync = 0xFF,
    Relative_Pos_Sync = 0xEE
} sync_trigger_enum_t;

typedef enum
{
    RS232_mode = 0,
    CW_CCW_mode,
    Pulse_Direction_mode,
    Analog_mode,
} cmd_input_mode_enum_t;

typedef enum
{
    relative_mode = 0, /*Works as relative mode. Operates as incremental encoder.*/
    absolute_mode,     /*Works as absolute mode. At power up,
                        motors moves to absolute zero or stored zero position.
                        See Set ABS Origin command for details.*/
} enc_mode_enum_t;

typedef enum
{
    Position_Servo_Mode = 0, /*(default for Modbus)*/
    Speed_Servo_Mode,
    Torque_Servo_Mode,
} servo_mode_enum_t;

typedef enum
{
    Servo_Enabled = 0,
    Servo_Disabled, /*(Motor Free)*/
} servo_activation_enum_t;

typedef struct __attribute__((__packed__))
{
    u8_t cmd_input_mode : 2;
    u8_t encoder_mode : 1;
    u8_t servo_mode : 2;
    u8_t servo_enable_disable : 1;
    u8_t reserved : 2;
} drive_cfg_bit_t;

typedef union
{
    drive_cfg_bit_t bit;
    u8_t byte;
} drive_cfg_t;

typedef enum
{
    On_position = 0, /* |Pset - Pmotor| < = OnpositionRange*/
    Off_Position     /* motor busy |Pset - Pmotor| > OnPositionRange */
} pos_t;

typedef enum
{
    No_Alarm = 0,
    Motor_lost_phase_alarm, /* |Pset - Pmotor|>8192(steps), 180(deg) */
    Over_current_alarm,
    Overheat_alarm, /* Overpower alarm*/
    Error_CRC       /* Error for CRC code check, refuse to accept current command*/
} alarm_enum_t;

typedef enum
{
    motion_completed = 0, /*waiting for next motion*/
    motion_is_busy,
} motion_enum_t;

typedef struct __attribute__((__packed__))
{
    u8_t pos : 1;
    u8_t servo_stat : 1;
    u8_t alarm : 3;
    u8_t motion_stat : 1;
    u8_t reserved : 2;
} drive_stat_bit_t;

typedef union
{
    drive_stat_bit_t bit;
    u8_t byte;
} drive_stat_t;

typedef struct
{
    u8_t Main_Gain;
    u8_t Speed_Gain;
    u8_t Integration_Gain;
    u8_t Torque_Filter_Constant;
} set_param_group1_t;

typedef struct
{
    u8_t Max_Speed;
    u8_t Max_Acceleration;
} set_param_group2_t;

typedef struct
{
    u8_t On_Pos_Rage;
    u16_t gear_number;
    u16_t line_number;
} set_param_group3_t;

u8_t Servo_CMD_Diagnostic_counter(FDCAN_TxHeaderTypeDef *tx_header);
u8_t Servo_CMD_Echo_Function(FDCAN_TxHeaderTypeDef *tx_header, u8_t *data);
u8_t Servo_CMD_Drive_Error(FDCAN_TxHeaderTypeDef *tx_header);
u8_t Servo_CMD_Go_Relative_Pos_PD(FDCAN_TxHeaderTypeDef *tx_header, s16_t pos_direct, u8_t *data);
u8_t Servo_CMD_Read_Motor_Speed(FDCAN_TxHeaderTypeDef *tx_header);
u8_t Servo_CMD_Servo_Disable(FDCAN_TxHeaderTypeDef *tx_header, servo_activation_enum_t servo_activation, u8_t *data);
u8_t Servo_CMD_Sync_Trigger(FDCAN_TxHeaderTypeDef *tx_header, sync_trigger_enum_t sync_trigger, u8_t *data);
u8_t Servo_CMD_Go_Relative_Pos_Sync(FDCAN_TxHeaderTypeDef *tx_header, s32_t rel_pos, u8_t *data);
u8_t Servo_CMD_Go_Absolute_Pos_Sync(FDCAN_TxHeaderTypeDef *tx_header, s32_t abs_pos, u8_t *data);
u8_t Servo_CMD_Read_Param_Group3(FDCAN_TxHeaderTypeDef *tx_header);
u8_t Servo_CMD_Read_Param_Group2(FDCAN_TxHeaderTypeDef *tx_header);
u8_t Servo_CMD_Read_Param_Group1(FDCAN_TxHeaderTypeDef *tx_header);
u8_t Servo_CMD_Set_Param_Group3(FDCAN_TxHeaderTypeDef *tx_header, set_param_group3_t set_param3, u8_t *data);
u8_t Servo_CMD_Set_Param_Group2(FDCAN_TxHeaderTypeDef *tx_header, set_param_group2_t set_param2, u8_t *data);
u8_t Servo_CMD_Set_Param_Group1(FDCAN_TxHeaderTypeDef *tx_header, set_param_group1_t set_param1, u8_t *data);
u8_t Servo_CMD_General_Read(FDCAN_TxHeaderTypeDef *tx_header, u8_t *data);
u8_t Servo_CMD_SS_Frequency(FDCAN_TxHeaderTypeDef *tx_header, u8_t freq, u8_t *data);
u8_t Servo_CMD_Sin_Wave(FDCAN_TxHeaderTypeDef *tx_header, s32_t amplitude, u8_t *data);
u8_t Servo_CMD_Square_Wave(FDCAN_TxHeaderTypeDef *tx_header, s32_t amplitude, u8_t *data);
u8_t Servo_CMD_Turn_ConstSpeed(FDCAN_TxHeaderTypeDef *tx_header, s16_t speed, u8_t *data);
u8_t Servo_CMD_Read_Drive_Status(FDCAN_TxHeaderTypeDef *tx_header);
u8_t Servo_CMD_Read_Drive_Config(FDCAN_TxHeaderTypeDef *tx_header);
u8_t Servo_CMD_Set_Drive_Config(FDCAN_TxHeaderTypeDef *tx_header, drive_cfg_t drive_cfg, u8_t *data);
u8_t Servo_CMD_Read_Drive_ID(FDCAN_TxHeaderTypeDef *tx_header);
u8_t Servo_CMD_Assign_Drive_ID(FDCAN_TxHeaderTypeDef *tx_header, u8_t drive_id, u8_t *data);
u8_t Servo_CMD_Make_CircularArc(FDCAN_TxHeaderTypeDef *tx_header, s32_t coordinate, u8_t *data);
u8_t Servo_CMD_Go_Relative_Pos_PTP(FDCAN_TxHeaderTypeDef *tx_header, s32_t rel_pos, u8_t *data);
u8_t Servo_CMD_Make_LinearLine(FDCAN_TxHeaderTypeDef *tx_header, s32_t coordinate, u8_t *data);
u8_t Servo_CMD_Go_Absolute_Pos_PTP(FDCAN_TxHeaderTypeDef *tx_header, s32_t abs_pos, u8_t *data);
u8_t Servo_CMD_Set_Origin(FDCAN_TxHeaderTypeDef *tx_header, u8_t *data);
void Servo_CMD_Init(FDCAN_TxHeaderTypeDef *tx_header);

#endif
