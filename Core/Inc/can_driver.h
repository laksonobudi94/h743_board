#ifndef __CAN_DRIVER
#define __CAN_DRIVER

#include "lwip/opt.h"
#include "stdio.h"
#include "string.h"

#include "lwip.h"
#include "lwip/init.h"
#include "lwip/netif.h"
#include "lwip/api.h"

#if defined(STM32H7)
#include "FreeRTOS.h"
#include "semphr.h"
#include "timers.h"
#endif

void init_CAN_thread(void *arg);

#endif
