#ifndef __SUPERVISION
#define __SUPERVISION

#include "FreeRTOS.h"
#include "event_groups.h"

#include "cmd_process.h"

#define mainLIST_BUFFER_SIZE 1024

typedef enum
{
    CREATE_THD_CLIENT = 0,
    CLOSE_THD_CLIENT,
    REPLY_CREATE_THD_CLIENT,
    REPLY_CLOSE_THD_CLIENT,
    CREATE_THD_LISTEN_PORT,
    CLOSE_THD_LISTEN_PORT,
    REPLY_CREATE_THD_LISTEN_PORT,
    REPLY_CLOSE_THD_LISTEN_PORT,
} cmd_spv_thd_t;

void task_list(int argc, char **argv);
BaseType_t check_bit_status_empty(EventBits_t status_bits);
u8_t count_num_of_flag_set(EventBits_t status_bits);
u8_t CreateClientThread(struct netconn *new_netconn);
u8_t CreateListenPortThd(struct netconn *obj_listen_port);
u8_t CloseClientThread(osThreadId id, struct netconn *delete_netconn);
u8_t create_supervision_thd(osThreadId thd_created, void *fn_client, void *fn_listenPort);

#endif
