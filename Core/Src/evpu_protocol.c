
/*
    Giga-Byt_55 50240
    DallasSe_24 10000
*/

#include "evpu_protocol.h"
#include "stdio.h"
#include "FreeRTOS.h"
#include "event_groups.h"
#include "conv_cam.h"
#include "can_driver.h"

extern u8_t one_time;

static u8_t buffProcess[MAX_RX_SIZE_BUFF];
static u8_t RxBuf[RxBuf_SIZE];
static u8_t SerialBuf[MainBuf_SIZE];

EventGroupHandle_t ser485EvGroup;

extern UART_HandleTypeDef huart2;

TimerHandle_t xTimers;

SemaphoreHandle_t xSemETH;
SemaphoreHandle_t xSemUart;

osMessageQueueId_t qUart_Rx_IT;
osMessageQueueId_t qReplyUart_Rx_IT;

static struct netconn *objTcpEvpu;

#define RE_ENABLE() HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, GPIO_PIN_RESET)
#define DE_ENABLE() HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, GPIO_PIN_SET)

static u8_t buff_temp1[MAX_LEN_SEND] = {0};

#define ARRAY_LEN(x) (sizeof(x) / sizeof((x)[0]))

#define DUMMY_DATA
#ifdef DUMMY_DATA
periodic_dat_t LiveData = {
    .set_azimuth_pos = 0, /*set point position*/
    .set_elevate_pos = 0, /*set point position*/
    .azimuth_pos_now = 0, /*from encoder*/
    .elevace_pos_now = 0, /*from encoder*/
    .move_azimut = 0,     /*azimuth_pos_now - last a val*/
    .move_elevate = 0,    /*elevate_pos_now - last e val*/
    .vsupply = 204,       /*ADC*/
};

cfg_t def_cfg_evpu = {
    .max_elev_deg = 45379, /*80*/
    .min_elev_deg = 17453, /*-80*/
    .a_sync_stat = 0,
    .e_sync_stat = 0,
    .period_send_p_t = 25, /*in milli second*/
    .max_azimuth_vel_limit = 7000,
    .max_elevate_vel_limit = 7000};
#endif

SemaphoreHandle_t get_sem_obj_ETH(void)
{
    return xSemETH;
}

SemaphoreHandle_t get_sem_obj_Uart(void)
{
    return xSemUart;
}

periodic_dat_t *get_pos_now(void)
{
    return &LiveData;
}

void init_usart_rx_thread(void *arg)
{
#define LEN_RX_QUEUE 10
    qUart_Rx_IT = osMessageQueueNew(LEN_RX_QUEUE, sizeof(void *), NULL);
    qReplyUart_Rx_IT = osMessageQueueNew(LEN_RX_QUEUE, sizeof(void *), NULL);

    osThreadNew(usart_rx_IT_thread, NULL, NULL);
    osThreadExit();
}

void usart_rx_IT_thread(void *arg)
{
    uint16_t Size;
    osStatus_t qStatus;
    HAL_StatusTypeDef status;

    status = HAL_UARTEx_ReceiveToIdle_IT(&huart2, RxBuf, sizeof(RxBuf));
    if (status != HAL_OK)
    {
        /*TODO: Error Handling*/
        while (1)
            osDelay(1000);
    }

    while (1)
    {
        osDelay(1);
        qStatus = osMessageQueueGet(qUart_Rx_IT, (void *)&Size, NULL, osWaitForever);
        if (qStatus != osOK)
            continue;

        status = HAL_UARTEx_ReceiveToIdle_IT(&huart2, RxBuf, sizeof(RxBuf));
        if (status != HAL_OK)
            continue;

        if (Size == 0)
            continue;

        qStatus = osMessageQueuePut(qReplyUart_Rx_IT, (void *)&Size, 0, 10);
        if (qStatus != osOK)
            continue;
    }
}

EventBits_t wait_usart_flag(EventBits_t flag)
{
    EventBits_t ser485bit;

    ser485bit = xEventGroupGetBits(ser485EvGroup);
    if (CHECK_BIT(ser485bit, flag) != 0)
        return ser485bit;

    ser485bit = xEventGroupWaitBits(
        ser485EvGroup,
        flag,
        pdTRUE,
        pdFALSE,
        osWaitForever);

    if (CHECK_BIT(ser485bit, flag) != 0)
        return ser485bit;

    return ser485bit;
}

u8_t Send_UART(u8_t *buff, u16_t len)
{
    DE_ENABLE();
    if (HAL_UART_Transmit(&huart2, buff, len, 1000) != HAL_OK)
        return pdFAIL;

    RE_ENABLE();

    return pdPASS;
}

u8_t Receive_UART_IT(u8_t *buff, u16_t *data_received)
{
    osStatus_t qStatus;
    u16_t Size;

    // wait response from dma until receive a data in idle
    qStatus = osMessageQueueGet(qReplyUart_Rx_IT,
                                (void *)&Size,
                                NULL,
                                TIMEOUT_RECV);

    if (qStatus != osOK)
        return pdFAIL;

    memcpy(buff, RxBuf, Size);
    *data_received = Size;

    return pdPASS;
}

static u8_t Set_Pos_PanTilt(periodic_dat_t *pos)
{
    u16_t len;
    cfg_t *pData;
    u16_t visca_value_pan, visca_value_tilt;

    pData = get_cfg_mainboard();
    memset(SerialBuf, 0, sizeof(SerialBuf));

    if (pos->set_elevate_pos > pData->max_elev_deg)
        pos->set_elevate_pos = pData->max_elev_deg;
    if (pos->set_elevate_pos < pData->min_elev_deg)
        pos->set_elevate_pos = pData->min_elev_deg;

    visca_value_pan = (u16_t)map_s32_to_s16((s32_t)pos->set_azimuth_pos,
                                            0,
                                            USHRT_MAX,
                                            MINUS_170_DEGREE,
                                            PLUS_170_DEGREE);
    visca_value_tilt = (u16_t)map_s32_to_s16((s32_t)pos->set_elevate_pos,
                                             pData->min_elev_deg,
                                             pData->max_elev_deg,
                                             MINUS_30_DEGREE,
                                             PLUS_90_DEGREE);

#if 1
    s16_t pan_angle = 0, tilt_angle = 0;
    pan_angle = map_s16((s16_t)visca_value_pan,
                        (s16_t)MINUS_170_DEGREE,
                        (s16_t)PLUS_170_DEGREE,
                        (s16_t)-170,
                        (s16_t)170);
    tilt_angle = map_s16((s16_t)visca_value_tilt,
                         (s16_t)MINUS_30_DEGREE,
                         (s16_t)PLUS_90_DEGREE,
                         (s16_t)-30,
                         (s16_t)90);
#endif
    len = CMD_Set_PT_Position(AbsolutePosition,
                              visca_value_pan,
                              visca_value_tilt,
                              MAX_HIGH_SPEED,
                              MAX_HIGH_SPEED,
                              SerialBuf);

    if (len == 0)
        return pdFAIL;

#if 1
    printf("Send Serial -> ");
    for (u16_t a = 0; a < len; a++)
        printf("%02x ", *(SerialBuf + a));
    printf("\r\n");
#endif

    if (CMD_TransReceive(SerialBuf, &len, SEM_TIMEOUT) != pdPASS)
        return pdFAIL;

    if (len == 0)
        return pdFAIL;

#if 0
    printf("\r\n\r\n");
    printf("A [u16 => %d [0x%x] | visc 0x%x | deg %d]\r\n",
           pos->set_azimuth_pos,
           pos->set_azimuth_pos,
           visca_value_pan,
           pan_angle);

    printf("E [u16 => %d [0x%x] | visc 0x%x | deg %d]\r\n",
           pos->set_elevate_pos,
           pos->set_elevate_pos,
           visca_value_tilt,
           tilt_angle);
    printf("\r\n\r\n");
#endif

#if 1
    /*TODO: must be check ACK or not*/
    printf("Reply Serial -> ");
    for (u16_t a = 0; a < len; a++)
        printf("%02x ", *(SerialBuf + a));
    printf("\r\n");
#endif

    return pdPASS;
}

#if 0
static u8_t Move_PanTilt(periodic_dat_t *pos)
{
    u8_t pan_speed = 0, tilt_speed = 0;
    u16_t len = 0;
    cfg_t *pData = NULL;

    pData = get_cfg_mainboard();
    memset(SerialBuf, 0, sizeof(SerialBuf));

    printf("Move -> azimut %d elevate %d\r\n", pos->move_azimut, pos->move_elevate);

    if (pos->move_azimut == 0 && pos->move_elevate == 0)
    {
        printf("Move Camera Stop\r\n");
#ifdef CONV_CAM_USE
        len = CMD_Pan_Tilt_Drive(Stop, SerialBuf, MAX_HIGH_SPEED, MAX_HIGH_SPEED);
#endif
        if (len == 0)
            return pdFAIL;
    }
    else if (pos->move_azimut > 0 && pos->move_elevate == 0)
    {
        printf("Move Camera Rotate Right\r\n");
#ifdef CONV_CAM_USE
        pan_speed = (u8_t)map_s16_to_s32((s16_t)pos->move_azimut,
                                                 0,
                                                 (s16_t)pData->max_azimuth_vel_limit,
                                                 (s32_t)MIN_LOW_SPEED,
                                                 (s32_t)MAX_HIGH_SPEED);
        len = CMD_Pan_Tilt_Drive(Right, SerialBuf, pan_speed, 0);
#endif
        if (len == 0)
            return pdFAIL;
    }
    else if (pos->move_azimut < 0 && pos->move_elevate == 0)
    {
        printf("Move Camera Rotate Left\r\n");
#ifdef CONV_CAM_USE
        pan_speed = (u8_t)map_s16_to_s32((s16_t)pos->move_azimut * -1,
                                                 0,
                                                 (s16_t)pData->max_azimuth_vel_limit,
                                                 (s32_t)MIN_LOW_SPEED,
                                                 (s32_t)MAX_HIGH_SPEED);
        len = CMD_Pan_Tilt_Drive(Left, SerialBuf, pan_speed, 0);
#endif
        if (len == 0)
            return pdFAIL;
    }
    else if (pos->move_azimut == 0 && pos->move_elevate > 0)
    {
        printf("Move Camera Rotate Up\r\n");
#ifdef CONV_CAM_USE
        tilt_speed = (u8_t)map_s16_to_s32((s16_t)pos->move_elevate,
                                                  0,
                                                  (s16_t)pData->max_elevate_vel_limit,
                                                  (s32_t)MIN_LOW_SPEED,
                                                  (s32_t)MAX_HIGH_SPEED);
        len = CMD_Pan_Tilt_Drive(Up, SerialBuf, 0, tilt_speed);
#endif
        if (len == 0)
            return pdFAIL;
    }
    else if (pos->move_azimut == 0 && pos->move_elevate < 0)
    {
        printf("Move Camera Rotate Down\r\n");
#ifdef CONV_CAM_USE
        tilt_speed = (u8_t)map_s16_to_s32((s16_t)pos->move_elevate * -1,
                                                  0,
                                                  (s16_t)pData->max_elevate_vel_limit,
                                                  (s32_t)MIN_LOW_SPEED,
                                                  (s32_t)MAX_HIGH_SPEED);
        len = CMD_Pan_Tilt_Drive(Down, SerialBuf, 0, tilt_speed);
#endif
        if (len == 0)
            return pdFAIL;
    }
    else if (pos->move_azimut > 0 && pos->move_elevate < 0)
    {
        printf("Move Camera Rotate Down Rigth\r\n");
#ifdef CONV_CAM_USE
        pan_speed = (u8_t)map_s16_to_s32((s16_t)pos->move_azimut,
                                                 0,
                                                 (s16_t)pData->max_azimuth_vel_limit,
                                                 (s32_t)MIN_LOW_SPEED,
                                                 (s32_t)MAX_HIGH_SPEED);
        tilt_speed = (u8_t)map_s16_to_s32((s16_t)pos->move_elevate * -1,
                                                  0,
                                                  (s16_t)pData->max_elevate_vel_limit,
                                                  (s32_t)MIN_LOW_SPEED,
                                                  (s32_t)MAX_HIGH_SPEED);
        len = CMD_Pan_Tilt_Drive(Downright, SerialBuf, pan_speed, tilt_speed);
#endif
        if (len == 0)
            return pdFAIL;
    }
    else if (pos->move_azimut < 0 && pos->move_elevate < 0)
    {
        printf("Move Camera Rotate Down Left\r\n");
#ifdef CONV_CAM_USE
        pan_speed = (u8_t)map_s16_to_s32((s16_t)pos->move_azimut * -1,
                                                 0,
                                                 (s16_t)pData->max_azimuth_vel_limit,
                                                 (s32_t)MIN_LOW_SPEED,
                                                 (s32_t)MAX_HIGH_SPEED);
        tilt_speed = (u8_t)map_s16_to_s32((s16_t)pos->move_elevate * -1,
                                                  0,
                                                  (s16_t)pData->max_elevate_vel_limit,
                                                  (s32_t)MIN_LOW_SPEED,
                                                  (s32_t)MAX_HIGH_SPEED);
        len = CMD_Pan_Tilt_Drive(Downleft, SerialBuf, pan_speed, tilt_speed);
#endif
        if (len == 0)
            return pdFAIL;
    }
    else if (pos->move_azimut > 0 && pos->move_elevate > 0)
    {
        printf("Move Camera Rotate Up Right\r\n");
#ifdef CONV_CAM_USE
        pan_speed = (u8_t)map_s16_to_s32((s16_t)pos->move_azimut,
                                                 0,
                                                 (s16_t)pData->max_azimuth_vel_limit,
                                                 (s32_t)MIN_LOW_SPEED,
                                                 (s32_t)MAX_HIGH_SPEED);
        tilt_speed = (u8_t)map_s16_to_s32((s16_t)pos->move_elevate,
                                                  0,
                                                  (s16_t)pData->max_elevate_vel_limit,
                                                  (s32_t)MIN_LOW_SPEED,
                                                  (s32_t)MAX_HIGH_SPEED);
        len = CMD_Pan_Tilt_Drive(Upright, SerialBuf, pan_speed, tilt_speed);
#endif
        if (len == 0)
            return pdFAIL;
    }
    else if (pos->move_azimut < 0 && pos->move_elevate > 0)
    {
        printf("Move Camera Rotate Up Left\r\n");
#ifdef CONV_CAM_USE
        pan_speed = (u8_t)map_s16_to_s32((s16_t)pos->move_azimut * -1,
                                                 0,
                                                 (s16_t)pData->max_azimuth_vel_limit,
                                                 (s32_t)MIN_LOW_SPEED,
                                                 (s32_t)MAX_HIGH_SPEED);
        tilt_speed = (u8_t)map_s16_to_s32((s16_t)pos->move_elevate,
                                                  0,
                                                  (s16_t)pData->max_elevate_vel_limit,
                                                  (s32_t)MIN_LOW_SPEED,
                                                  (s32_t)MAX_HIGH_SPEED);
        len = CMD_Pan_Tilt_Drive(Upleft, SerialBuf, pan_speed, tilt_speed);
#endif
        if (len == 0)
            return pdFAIL;
    }

    printf("Send Serial -> ");
    for (u16_t a = 0; a < len; a++)
        printf("%02x ", *(SerialBuf + a));
    printf("\r\n");

    if (xSemaphoreTake(xSemUart, (TickType_t)1000) == pdTRUE)
    {
        if (Send_UART(SerialBuf, len) != pdPASS)
            return pdFAIL;
        if (Receive_UART_IT(SerialBuf, &len) != pdPASS)
        {
            xSemaphoreGive(xSemUart);
            return pdFAIL;
        }
        xSemaphoreGive(xSemUart);
    }
    else
        return pdFAIL;

    if (len == 0)
        return pdFAIL;

    printf("Reply Serial -> ");
    for (u16_t a = 0; a < len; a++)
        printf("%02x ", *(SerialBuf + a));
    printf("\r\n");

    return pdPASS;
}
#endif

err_t write_netconn_sem(struct netconn *objTcp, u8_t *buff, u16_t len)
{
    if (objTcp == NULL)
        return ERR_VAL;

    if (xSemaphoreTake(xSemETH, (TickType_t)SEM_TIMEOUT) != pdTRUE)
        return ERR_TIMEOUT;

    if (netconn_write(objTcp, buff, len, NETCONN_COPY) != ERR_OK)
    {
        if (xSemaphoreGive(xSemETH) != pdPASS)
        {
            if (xSemaphoreGive(xSemETH) != pdPASS)
                return ERR_USE;
        }

        return ERR_CONN;
    }

    if (xSemaphoreGive(xSemETH) != pdPASS)
        return ERR_USE;

    return ERR_OK;
}

u8_t sync_process(void)
{
    char sendBuff[100], tempBuff[100];
    uint16_t len;
    cfg_t *pCfg;

    memset(sendBuff, 0, sizeof(sendBuff));
    memset(tempBuff, 0, sizeof(tempBuff));

    printf("%s...\r\n", __func__);

    pCfg = get_cfg_mainboard();

    sprintf((char *)tempBuff, "%s %s",
            AZIMUTH_SYNC_CMD,
            pCfg->a_sync_stat > 0 ? "ok" : "fail");

    sprintf(sendBuff, "%s %s %d %s", MAINBOARD_SER, CMD_DAT, strlen(tempBuff), tempBuff);
    len = strlen((char *)sendBuff);
    if (len == 0)
        return pdFAIL;

    // last LFCR from MCU
    sendBuff[len++] = (END_MSG_MCU & 0xFF00) >> 8;
    sendBuff[len++] = END_MSG_MCU & 0x00FF;

    if (write_netconn_sem(objTcpEvpu, (u8_t *)sendBuff, len) != ERR_OK)
        return pdFAIL;

    memset(sendBuff, 0, sizeof(sendBuff));
    memset(tempBuff, 0, sizeof(tempBuff));

    sprintf((char *)tempBuff, "%s %s",
            ELEVATE_SYNC_CMD,
            pCfg->e_sync_stat > 0 ? "ok" : "fail");

    sprintf(sendBuff, "%s %s %d %s",
            MAINBOARD_SER,
            CMD_DAT,
            strlen(tempBuff),
            tempBuff);

    len = strlen((char *)sendBuff);

    // last LFCR from MCU
    sendBuff[len++] = (END_MSG_MCU & 0xFF00) >> 8;
    sendBuff[len++] = END_MSG_MCU & 0x00FF;

    if (write_netconn_sem(objTcpEvpu, (u8_t *)sendBuff, len) != ERR_OK)
        return pdFAIL;

    return pdPASS;
}

void periodic_PT_pos(TimerHandle_t xTimer)
{
    /*TODO: when disconected must be stoped timer,
        when connected again must be sync again to set timer period callback*/

    /*TODO: in wireshark wile sending timestamp not precision*/
    u16_t len, len_serial;
    char sendBuff[100], tempBuff[100];
    periodic_dat_t *pData;
    s16_t pan_angle, tilt_angle;
    s16_t sp_pan_angle, sp_tilt_angle;
    s16_t diff_pan_angle, diff_tilt_angle;
    u16_t pan_visca_pos, tilt_visca_pos;
    cfg_t *pCfg;

    memset(sendBuff, 0, sizeof(sendBuff));
    memset(tempBuff, 0, sizeof(tempBuff));

    azimuth_arrow_t a_arrow_move = NO_SET_POS_AZIMUTH;
    elevate_arrow_t e_arrow_move = NO_SET_POS_ELEVATE;

#if 0
    printf("%s => %d tick\r\n", __func__, xTaskGetTickCount());
#endif

    memset(SerialBuf, 0, sizeof(SerialBuf));

    pCfg = get_cfg_mainboard();
    if ((pCfg == NULL) | (xSemETH == NULL) | (xSemUart == NULL))
        return;

    pData = get_pos_now();

    len_serial = CMD_Get_PT_Position(SerialBuf);
    if (len_serial == 0)
        return;

    if (CMD_TransReceive(SerialBuf, &len_serial, 1) == pdPASS)
    {

#if 0
    printf("Reply Serial -> ");
    for (u16_t a = 0; a < len_serial; a++)
        printf("%02x ", *(SerialBuf + a));
    printf("\r\n");
#endif

        if (Parse_Get_PT_Position(SerialBuf, &pan_visca_pos, &tilt_visca_pos) != pdPASS)
            return;

        pan_angle = map_s16((s16_t)pan_visca_pos,
                            MINUS_170_DEGREE,
                            PLUS_170_DEGREE,
                            -170,
                            170);
        tilt_angle = map_s16((s16_t)tilt_visca_pos,
                             MINUS_30_DEGREE,
                             PLUS_90_DEGREE,
                             -30,
                             90);

        pData->azimuth_pos_now = (u16_t)map_s16_to_s32((s16_t)pan_visca_pos,
                                                       MINUS_170_DEGREE,
                                                       PLUS_170_DEGREE,
                                                       0,
                                                       65535);
        pData->elevace_pos_now = (u16_t)map_s16_to_s32((s16_t)tilt_visca_pos,
                                                       MINUS_30_DEGREE,
                                                       PLUS_90_DEGREE,
                                                       pCfg->min_elev_deg,
                                                       pCfg->max_elev_deg);

        sp_pan_angle = map_s32_to_s16((s16_t)pData->set_azimuth_pos,
                                      0,
                                      65535,
                                      -170,
                                      170);
        sp_tilt_angle = map_s32_to_s16((s16_t)pData->set_elevate_pos,
                                       (s32_t)pCfg->min_elev_deg,
                                       (s32_t)pCfg->max_elev_deg,
                                       -30,
                                       90);

        diff_pan_angle = sp_pan_angle - pan_angle;
        diff_tilt_angle = sp_tilt_angle - tilt_angle;

        if (diff_pan_angle < 0)
        {
            a_arrow_move = LEFT_MOVE_AZIMUTH;
            diff_pan_angle *= -1;
        }
        if (diff_tilt_angle < 0)
        {
            e_arrow_move = DOWN_MOVE_ELEVATE;
            diff_tilt_angle *= -1;
        }

#if 0
    printf("\r\nPos Now P => %d deg | T => %d deg\r\n", pan_angle, tilt_angle);
    printf("Set Pos P => %d deg | T => %d deg\r\n", sp_pan_angle, sp_tilt_angle);
    printf("Diff Pos P => %d deg | T => %d deg\r\n", diff_pan_angle, diff_tilt_angle);
#endif

        if (pData->set_azimuth_pos == 0 & pData->set_elevate_pos == 0)
        {
            // this indicate never be set position
            pData->move_azimut = 0;
            pData->move_elevate = 0;
            a_arrow_move = NO_SET_POS_AZIMUTH;
            e_arrow_move = NO_SET_POS_ELEVATE;
        }
        else
        {
            pData->move_azimut = (u16_t)map_s16_to_s32(diff_pan_angle,
                                                       0,
                                                       340,
                                                       0,
                                                       65535);
            pData->move_elevate = (u16_t)map_s16_to_s32(diff_tilt_angle,
                                                        0,
                                                        120,
                                                        0,
                                                        (s32_t)(pCfg->max_elev_deg - pCfg->min_elev_deg));
        }
    }
    else
    {
        printf("Warn: Serial Not Available!\r\n");
    }

    /*TODO: encoder must be check*/
    sprintf((char *)tempBuff, "%s %05d %05d %c%04d %c%04d %d %016llu",
            SEND_PERIOD_PT_POS,
            pData->azimuth_pos_now,
            pData->elevace_pos_now,
            a_arrow_move == LEFT_MOVE_AZIMUTH ? '-' : '+',
            pData->move_azimut,
            e_arrow_move == DOWN_MOVE_ELEVATE ? '-' : '+',
            pData->move_elevate,
            pData->vsupply,
            pData->status_alarm);

    sprintf(sendBuff, "%s %s %d %s", MAINBOARD_SER, CMD_DAT, strlen(tempBuff), tempBuff);
    len = strlen((char *)sendBuff);

    // last LFCR from MCU
    sendBuff[len++] = (END_MSG_MCU & 0xFF00) >> 8;
    sendBuff[len++] = END_MSG_MCU & 0x00FF;

    err_t err = write_netconn_sem(objTcpEvpu, (u8_t *)sendBuff, len);

    if (err != ERR_OK)
        printf("err %d\r\n", err);

    return;
}

void set_obj_evpu_tcp(struct netconn *tcp)
{
    objTcpEvpu = tcp;
}

u8_t clear_obj_evpu_tcp(void)
{
    cfg_t *pData;

    pData = get_cfg_mainboard();
    one_time = 0;

    if (objTcpEvpu == NULL)
        return pdFAIL;

    objTcpEvpu = NULL;

    if (pData->period_send_p_t != 0 && xTimerIsTimerActive(xTimers) == pdTRUE)
        xTimerStop(xTimers, 0);

    return pdPASS;
}

init_evpu_err_t init_evpu(void)
{
    cfg_t *pCfg;
    TickType_t periodic_tim;
    periodic_dat_t *pPos;

    pCfg = get_cfg_mainboard();

    ser485EvGroup = xEventGroupCreate();
    if (ser485EvGroup == NULL)
        return CREATE_EVENT_485_ERR;

    xSemETH = xSemaphoreCreateBinary();
    if (xSemETH == NULL)
        return CREATE_SEM_ETH_ERR;
    if (xSemaphoreGive(xSemETH) != pdTRUE)
        printf("Warn: xSemETH was released!\r\n");

    xSemUart = xSemaphoreCreateBinary();
    if (xSemUart == NULL)
        return CREATE_SEM_UART_ERR;
    if (xSemaphoreGive(xSemUart) != pdTRUE)
        printf("Warn: xSemUart was released!\r\n");

    periodic_tim = (TickType_t)(pCfg->period_send_p_t * 10) / portTICK_PERIOD_MS;
    printf("periodic_tim = %d\r\n", periodic_tim);

    xTimers = xTimerCreate(
        "PeriodicPT",
        periodic_tim,
        pdTRUE,
        (void *)0,
        periodic_PT_pos);

    if (xTimers == NULL)
        return CREATE_TIMER_ERR;

#if 0
    // for testing
    if (xTimerChangePeriod(xTimers, (pCfg->period_send_p_t * 10) / portTICK_PERIOD_MS, 100) == pdPASS)
    {
        if (xTimerIsTimerActive(xTimers) == pdFALSE)
        {
            printf("xTimerStart... \r\n");
            xTimerStart(xTimers, 0);
        }
    }
#endif

    RE_ENABLE();
    if (osThreadNew(init_usart_rx_thread, NULL, NULL) == NULL)
        return CREATE_THD_RX_UART_ERR;

    if (osThreadNew(init_CAN_thread, NULL, NULL) == NULL)
        return CREATE_THD_RX_CAN_ERR;

    // when first powering up , set home position
    if (set_home_PT() != pdPASS)
        return CREATE_SET_HOME_PT_ERR;

    pPos = get_pos_now();
    if (pPos == NULL)
        return GET_OBJ_POS_ERR;

    if (get_encoder_pos((u16_t *)&pPos->azimuth_pos_now,
                        (u16_t *)&pPos->elevace_pos_now,
                        NULL,
                        NULL) != pdPASS)
    {
        return GET_ENCODER_PT_ERR;
    }

    return INIT_EVPU_SUCCESS;
}

u8_t CMD_TransReceive(u8_t *buff, u16_t *len, TickType_t tick_timeout)
{
    u16_t recv_len;
    u16_t send_len = *len;

    if (xSemaphoreTake(xSemUart, tick_timeout) != pdTRUE)
    {
        printf("Err: xSemaphoreTake xSemUart not available!\r\n");
        return pdFAIL;
    }

    if (Send_UART(buff, send_len) != pdPASS)
    {
        printf("Err: Send_UART fail!\r\n");
        return pdFAIL;
    }

    if (Receive_UART_IT(buff, &recv_len) != pdPASS)
    {
        printf("Err: Timeout Serial!\r\n");
        xSemaphoreGive(xSemUart);
        return pdFAIL;
    }

    *len = recv_len;

    if (xSemaphoreGive(xSemUart) != pdPASS)
    {
        printf("Err: xSemaphoreGive xSemUart not available!\r\n");
        return pdFAIL;
    }

    return pdPASS;
}

cfg_t *get_cfg_mainboard(void)
{
    return &def_cfg_evpu;
}

u8_t switch_video(u8_t vid_num, u8_t mode)
{
    printf("Switch %d -> %d\n\r", vid_num, mode);
    return pdPASS;
}

u8_t configure_serial(u8_t serNum, u16_t baud, u8_t databit, u8_t stop_bit, char parity)
{
    printf("Configure Serial %d -> %d %d %d %c\n\r", serNum, baud, databit, stop_bit, parity);
    return pdPASS;
}

u8_t IR_parse(u8_t *buff)
{
    ir_protocol_t ir_cmd;

    if (memcmp(buff, &ir_protocol_list[ZoomPlus], 8) == 0)
    {
        ir_cmd = ZoomPlus;
    }
    else if (memcmp(buff, &ir_protocol_list[ZoomStopFocusStop], 8) == 0)
    {
        ir_cmd = ZoomStopFocusStop;
    }
    else if (memcmp(buff, &ir_protocol_list[ZoomStopEnd], 4) == 0)
    {
        ir_cmd = ZoomStopEnd;
    }
    else if (memcmp(buff, &ir_protocol_list[ZoomMinus], 8) == 0)
    {
        ir_cmd = ZoomMinus;
    }
    else if (memcmp(buff, &ir_protocol_list[AutoFocus], 4) == 0)
    {
        ir_cmd = AutoFocus;
    }
    else if (memcmp(buff, &ir_protocol_list[FocusMinus], 8) == 0)
    {
        ir_cmd = FocusMinus;
    }
    else if (memcmp(buff, &ir_protocol_list[FocusPlus], 8) == 0)
    {
        ir_cmd = FocusPlus;
    }

    printf("cmd IR => %s\r\n", name_enum_ir[ir_cmd]);

    return pdPASS;
}

u8_t CCD_parse(u8_t *buff)
{
    visca_t visca_cmd;
    u8_t p_variable;

    if (memcmp(buff, &visca_protocol_list[CAM_Zoom_Tele], 4) == 0)
    {
        buff += 4;
        if ((*buff & 0xF0) == (visca_protocol_list[CAM_Zoom_Tele][4] & 0xF0))
        {
            visca_cmd = CAM_Zoom_Tele;
            p_variable = *buff & 0x0F;
        }
        else if (*buff == visca_protocol_list[CAM_Zoom_Stop][4])
        {
            visca_cmd = CAM_Zoom_Stop;
        }
        else if ((*buff & 0xF0) == (visca_protocol_list[CAM_Zoom_Wide][4] & 0xF0))
        {
            visca_cmd = CAM_Zoom_Wide;
            p_variable = *buff & 0x0F;
        }
    }
    else if (memcmp(buff, &visca_protocol_list[CAM_ZoomPosInq], 5) == 0)
    {
        visca_cmd = CAM_ZoomPosInq;
    }
    else if (memcmp(buff, &visca_protocol_list[CAM_Focus_Auto_Focus], 5) == 0)
    {
        visca_cmd = CAM_Focus_Auto_Focus;
    }
    else if (memcmp(buff, &visca_protocol_list[CAM_Focus_Manual_Focus], 5) == 0)
    {
        visca_cmd = CAM_Focus_Manual_Focus;
    }
    else if (memcmp(buff, &visca_protocol_list[CAM_Focus_Near], 4) == 0)
    {
        buff += 4;
        if ((*buff & 0xF0) == (visca_protocol_list[CAM_Focus_Near][4] & 0xF0))
        {
            visca_cmd = CAM_Focus_Near;
            p_variable = *buff & 0x0F;
        }
        else if (*buff == (u8_t)(visca_protocol_list[CAM_Focus_Stop][4]))
        {
            visca_cmd = CAM_Focus_Stop;
        }
        else if ((*buff & 0xF0) == (visca_protocol_list[CAM_Focus_Far][4] & 0xF0))
        {
            visca_cmd = CAM_Focus_Far;
            p_variable = *buff & 0x0F;
        }
    }

    printf("cmd CCD => %s %d\r\n", name_enum_ccd[visca_cmd], p_variable);

    return pdPASS;
}

u8_t send_serial(serial_t serID, u8_t *buff, u16_t len)
{
    u16_t length;
    printf("Pasthrough Serial %d [%d] -> ", serID, len);
    for (u16_t a = 0; a < len; a++)
        printf("%02x ", *(buff + a));

    printf("\n\r");

    switch (serID)
    {
    case SERIAL1:
        // CCD_parse(buff);

        DE_ENABLE();
        if (HAL_UART_Transmit_IT(&huart2, buff, len) != HAL_OK)
            return pdFAIL;
        wait_usart_flag(BIT_TX_COMPLETE);

        Receive_UART_IT(buffProcess, &length);
        printf("Reply Serial -> ");
        for (u16_t a = 0; a < length; a++)
            printf("%02x ", *(buffProcess + a));

        break;
    case SERIAL2:
        // IR_parse(buff);
        break;
    case SERIAL3:
        break;
    default:
        break;
    }
    return pdPASS;
}

u8_t serial_controll(u8_t *buff, u8_t id)
{
    char ownBuff[10];
    memset(ownBuff, 0, sizeof(ownBuff));

    buff += 3;
    if (memcmp(buff, (char *)CMD_DAT, 4) == 0)
    {
        buff += 5;
        u16_t length = (u16_t)atoi((char *)buff);
        buff += 2;
        return send_serial((serial_t)id, buff, length);
    }
    else if (memcmp(buff, (char *)SERIAL_CFG, 4) == 0)
    {
        buff += 5;
        u16_t baud = (u16_t)atoi((char *)buff);
        sprintf(ownBuff, "%d", baud);
        buff += strlen(ownBuff);
        buff++;
        u8_t databit = (u8_t)atoi((char *)buff);
        buff += 2;
        u8_t stopbit = (u8_t)atoi((char *)buff);
        buff += 2;
        char parity = *buff;
        return configure_serial(id, baud, databit, stopbit, parity);
    }
    return pdPASS;
}

u8_t mainboard_process(u8_t *buff, u16_t *len, u8_t *need_reply)
{
    cfg_t *pCfg;
    periodic_dat_t *posDat;
    char temp[10];
    size_t len_val;
    s16_t new_move_azimut, new_move_elevate;

    // we attemping data velo burst same value
    static s16_t temp_move_azimut = 0;
    static s16_t temp_move_elevate = 0;

    pCfg = get_cfg_mainboard();
    posDat = get_pos_now();

    // printf("%s\r\n", __func__);

    // handle multiple switch
    for (int a = 0; a < MAX_MULTIPLE_SW; a++)
    {
        if (*buff == 'o')
        {
            // SET_SWITCH
            buff += 2;
            u8_t vid_num = (*buff) - '0';
            buff += 2;
            u8_t mode = (*buff) - '0';
            switch_video(vid_num, mode);
        }
        else
        {
            if (a >= 1)
                return pdPASS;
            else
                break;
        }
        buff++;
        // for switch endl is swapped
        if (*(buff++) != (u8_t)(END_MSG_PC & 0x00FF))
            return pdFAIL;
        if (*(buff++) != (u8_t)((END_MSG_PC >> 8) & 0x00FF))
            return pdFAIL;
    }

    /*TODO: multiple data frame in one frame*/
    if (memcmp(buff, AZIMUTH_POSITION_CMD, strlen(AZIMUTH_POSITION_CMD)) == 0)
    {
        buff += (strlen(AZIMUTH_POSITION_CMD) + 1);
        posDat->set_azimuth_pos = (u32_t)atoi((char *)buff);
        sprintf(temp, "%d", posDat->set_azimuth_pos);
        len_val = strlen(temp);
        buff += len_val;

        if (*(buff++) != (u8_t)((END_MSG_PC >> 8) & 0x00FF))
            return pdFAIL;
        if (*(buff++) != (u8_t)(END_MSG_PC & 0x00FF))
            return pdFAIL;

        if (memcmp(buff, ELEVATE_POSITION_CMD, strlen(ELEVATE_POSITION_CMD)) == 0)
        {
            buff += (strlen(ELEVATE_POSITION_CMD) + 1);
            posDat->set_elevate_pos = (u16_t)atoi((char *)buff);
        }
        return Set_Pos_PanTilt(posDat);
    }
    else if (memcmp(buff, ELEVATE_POSITION_CMD, strlen(ELEVATE_POSITION_CMD)) == 0)
    {
        buff += (strlen(ELEVATE_POSITION_CMD) + 1);
        posDat->set_elevate_pos = (u16_t)atoi((char *)buff);
        sprintf(temp, "%d", posDat->set_elevate_pos);
        len_val = strlen(temp);
        buff += len_val;

        if (*(buff++) != (u8_t)((END_MSG_PC >> 8) & 0x00FF))
            return pdFAIL;
        if (*(buff++) != (u8_t)(END_MSG_PC & 0x00FF))
            return pdFAIL;

        if (memcmp(buff, AZIMUTH_POSITION_CMD, strlen(AZIMUTH_POSITION_CMD)) == 0)
        {
            buff += (strlen(AZIMUTH_POSITION_CMD) + 1);
            posDat->set_azimuth_pos = (u16_t)atoi((char *)buff);
        }
        return Set_Pos_PanTilt(posDat);
    }
    else if (memcmp(buff, AZIMUTH_VELO_CMD, strlen(AZIMUTH_VELO_CMD)) == 0)
    {
        buff += (strlen(AZIMUTH_VELO_CMD) + 1);
        new_move_azimut = (s16_t)atoi((char *)buff);
        check_position(new_move_azimut,
                       pCfg->max_azimuth_vel_limit,
                       posDat->azimuth_pos_now,
                       &temp_move_azimut,
                       &posDat->set_azimuth_pos,
                       &posDat->move_azimut);

        sprintf(temp, "%d", new_move_azimut);
        len_val = strlen(temp);
        buff += len_val;

        if (*(buff++) != (u8_t)((END_MSG_PC >> 8) & 0x00FF))
            return pdFAIL;
        if (*(buff++) != (u8_t)(END_MSG_PC & 0x00FF))
            return pdFAIL;

        if (memcmp(buff, ELEVATE_VELO_CMD, strlen(ELEVATE_VELO_CMD)) == 0)
        {
            buff += (strlen(ELEVATE_VELO_CMD) + 1);
            new_move_elevate = (s16_t)atoi((char *)buff);
            check_position(new_move_elevate,
                           pCfg->max_elevate_vel_limit,
                           posDat->elevace_pos_now,
                           &temp_move_elevate,
                           &posDat->set_elevate_pos,
                           &posDat->move_elevate);
        }
        return Set_Pos_PanTilt(posDat);
    }
    else if (memcmp(buff, ELEVATE_VELO_CMD, strlen(ELEVATE_VELO_CMD)) == 0)
    {
        buff += (strlen(ELEVATE_VELO_CMD) + 1);
        new_move_elevate = (s16_t)atoi((char *)buff);
        check_position(new_move_elevate,
                       pCfg->max_elevate_vel_limit,
                       posDat->elevace_pos_now,
                       &temp_move_elevate,
                       &posDat->set_elevate_pos,
                       &posDat->move_elevate);

        sprintf(temp, "%d", posDat->move_elevate);
        len_val = strlen(temp);
        buff += len_val;

        if (*(buff++) != (u8_t)((END_MSG_PC >> 8) & 0x00FF))
            return pdFAIL;
        // buff++; //what is this?
        if (*(buff++) != (u8_t)(END_MSG_PC & 0x00FF))
            return pdFAIL;

        if (memcmp(buff, AZIMUTH_VELO_CMD, strlen(AZIMUTH_VELO_CMD)) == 0)
        {
            buff += (strlen(AZIMUTH_VELO_CMD) + 1);
            new_move_azimut = (s16_t)atoi((char *)buff);
            check_position(new_move_azimut,
                           pCfg->max_azimuth_vel_limit,
                           posDat->azimuth_pos_now,
                           &temp_move_azimut,
                           &posDat->set_azimuth_pos,
                           &posDat->move_azimut);
        }
        return Set_Pos_PanTilt(posDat);
    }
    else if (memcmp(buff, AZIMUTH_VEL_LIMIT_CMD, strlen(AZIMUTH_VEL_LIMIT_CMD)) == 0)
    {
        buff += (strlen(AZIMUTH_VEL_LIMIT_CMD) + 1);
        pCfg->max_azimuth_vel_limit = (u16_t)atoi((char *)buff);
        printf("limit velocity azimuth => %d\r\n", pCfg->max_azimuth_vel_limit);
        return pdPASS;
    }
    else if (memcmp(buff, ELEVATE_VEL_LIMIT_CMD, strlen(ELEVATE_VEL_LIMIT_CMD)) == 0)
    {
        /*TODO max limit string val atoi*/
        buff += (strlen(ELEVATE_VEL_LIMIT_CMD) + 1);
        pCfg->max_elevate_vel_limit = (u16_t)atoi((char *)buff);
        printf("limit velocity elevate => %d\r\n", pCfg->max_azimuth_vel_limit);
        return pdPASS;
    }
    else
        __NOP();

    *need_reply = pdTRUE;

    if (memcmp(buff, REQ_ELEVATE_MIN_DEG, strlen(REQ_ELEVATE_MIN_DEG)) == 0)
        sprintf((char *)buff_temp1, "%s %d", REQ_ELEVATE_MIN_DEG, pCfg->min_elev_deg);
    else if (memcmp(buff, REQ_ELEVATE_MAX_DEG, strlen(REQ_ELEVATE_MAX_DEG)) == 0)
        sprintf((char *)buff_temp1, "%s %d", REQ_ELEVATE_MAX_DEG, pCfg->max_elev_deg);
    else if (memcmp(buff, REQ_AZIMUTH_SYNC_STAT, strlen(REQ_AZIMUTH_SYNC_STAT)) == 0)
        sprintf((char *)buff_temp1, "%s %d", AZIMUTH_SYNC_CMD, pCfg->a_sync_stat);
    else if (memcmp(buff, REQ_ELEVATE_SYNC_STAT, strlen(REQ_ELEVATE_SYNC_STAT)) == 0)
        sprintf((char *)buff_temp1, "%s %d", ELEVATE_SYNC_CMD, pCfg->e_sync_stat);
    else if (memcmp(buff, CFG_SEND_PERIOD_PT_POS, strlen(CFG_SEND_PERIOD_PT_POS)) == 0)
    {
        *need_reply = pdFALSE;

        pCfg->period_send_p_t = (u32_t)atoi((char *)buff + LEN_CMD_0);
        if (pCfg->period_send_p_t == 0 && xTimerIsTimerActive(xTimers) == pdTRUE)
            xTimerStop(xTimers, 0);
        if (pCfg->period_send_p_t > 0)
        {
            if (xTimerChangePeriod(xTimers, (pCfg->period_send_p_t * 10) / portTICK_PERIOD_MS, 100) == pdPASS)
            {
                if (xTimerIsTimerActive(xTimers) == pdFALSE)
                    xTimerStart(xTimers, 0);
            }
        }
    }
    else if (memcmp(buff, AZIMUTH_SYNC_CMD, strlen(AZIMUTH_SYNC_CMD)) == 0)
    {
        *need_reply = pdFALSE;
        buff += LEN_CMD_1;
        if (*(buff++) != (u8_t)((END_MSG_PC >> 8) & 0x00FF))
            return pdFAIL;
        if (*(buff++) != (u8_t)(END_MSG_PC & 0x00FF))
            return pdFAIL;

        if (memcmp(buff, ELEVATE_SYNC_CMD, strlen(ELEVATE_SYNC_CMD)) == 0)
        {
            if (sync_process() != pdPASS)
                return pdFAIL;
        }
    }
    else
        return pdFAIL;

    if (*need_reply == pdFALSE)
        return pdPASS;

    // be carefull in here tge pointer buff
    // has been shifted by 2 after checking prefix in front of process
    buff -= 2;
    sprintf((char *)buff, "%s %s %d %s",
            MAINBOARD_SER,
            CMD_DAT,
            strlen((char *)buff_temp1),
            buff_temp1);

    *len = strlen((char *)buff);

    // last LFCR from MCU
    buff[(*len)++] = (END_MSG_MCU & 0xFF00) >> 8;
    buff[(*len)++] = END_MSG_MCU & 0x00FF;

    return pdPASS;
}

u8_t evpu_parse_buff(u8_t *buff, u16_t *len, u8_t *need_reply)
{
    serial_t id_serial;
    u16_t lenght = *len;
    *need_reply = pdFALSE;

    // printf("%s\r\n", __func__);

    // end of data check
    // for switch is inverted [Software RCM is missed]
    // data command from PC without endl
    if ((*(buff + 2) != 'o') && (memcmp(buff + 2 + 3, (char *)CMD_DAT, 4) != 0))
    {
        if (*(buff + lenght - 2) != (u8_t)((END_MSG_PC >> 8) & 0x00FF))
            return pdFAIL;
        if (*(buff + lenght - 1) != (u8_t)(END_MSG_PC & 0x00FF))
            return pdFAIL;
    }

    if (*buff != (u8_t)((MY_PREFIX >> 8) & 0x00FF))
        return pdFAIL;

    buff++;
    if (*buff != (u8_t)(MY_PREFIX & 0x00FF))
        return pdFAIL;

    buff++;
    // handle serial passthrought
    if (memcmp(buff, DAY_CAM_SER, 2) == 0)
    {
        id_serial = SERIAL1;
        if (serial_controll(buff, id_serial) != pdPASS)
            return pdFAIL;
    }
    else if (memcmp(buff, IR_CAM_SER, 2) == 0)
    {
        id_serial = SERIAL2;
        if (serial_controll(buff, id_serial) != pdPASS)
            return pdFAIL;
    }
    else if (memcmp(buff, LRF_0_SER, 2) == 0)
    {
        id_serial = SERIAL3;
        if (serial_controll(buff, id_serial) != pdPASS)
            return pdFAIL;
    }
    else if (*buff != 's')
    {
        if (mainboard_process(buff, len, need_reply) != pdPASS)
            return pdFAIL;
    }
    else
        return pdFAIL;

    return pdPASS;
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
    uint16_t Size = 0;
    if (huart->Instance == USART2)
        osMessageQueuePut(qUart_Rx_IT, (void *)&Size, 0, 0);
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
    if (huart->Instance == USART2)
        osMessageQueuePut(qUart_Rx_IT, (void *)&Size, 0, 0);
}
