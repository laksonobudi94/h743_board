#include "supervision.h"
#include "flash_data.h"
#include "evpu_protocol.h"

QueueHandle_t qThdSpv, qReplyThdSpvClient, qReplyThdSpvLport;
EventGroupHandle_t tcpClientEvGroup, lPortThdEvGroup;
void *funcClientThd;
void *funcListenPortThd;
extern struct netconn *tcp_conn[MAX_TCP_CLIENT];

#if 0
void task_list(int argc, char **argv)
{
    (void)argc;
    (void)argv;
#if INCLUDE_vTaskSuspend == 0
    printf("\r\n--> task list NON AKTIF, INCLUDE_vTaskSuspend perlu diaktifkan.\r\n");
#else
    static char cListBuffer[mainLIST_BUFFER_SIZE];
    // const char *pcList = &( cListBuffer[ 0 ] );
    const char *const pcHeader = "Task          State  Priority  Stack	#\r\n************************************************\r\n";
    sprintf(cListBuffer, "\r\nJml Task: %ld,     ", uxTaskGetNumberOfTasks());
    printf(cListBuffer);
    printf("Memory space: %d\r\n", xPortGetFreeHeapSize());
    printf(pcHeader);
    vTaskList(cListBuffer);
    printf(cListBuffer);
    printf("\r\n");
#endif
}
#endif

BaseType_t check_bit_status_empty(EventBits_t status_bits)
{
    for (u32_t i = 0; i < MAX_TCP_CLIENT; i++)
    {
        if (CHECK_BIT(status_bits, i) == 0)
            return i;
    }

    return 255;
}

u8_t count_num_of_flag_set(EventBits_t status_bits)
{
    u8_t count = 0;
    for (u32_t i = 0; i < MAX_TCP_PORT; i++)
    {
        if (CHECK_BIT(status_bits, i) != 0)
            count++;
    }

    return count;
}

u8_t get_idx_with_compare(struct netconn *addr)
{
    for (u8_t i = 0; i < MAX_TCP_CLIENT; i++)
    {
        if (addr == tcp_conn[i])
            return i;
    }

    return 255;
}

u8_t CloseClientThread(osThreadId id, struct netconn *delete_netconn)
{
    Message send_msg;

    send_msg.payload_0 = CLOSE_THD_CLIENT;
    send_msg.payload_1 = (u32_t)delete_netconn;
    send_msg.payload_2 = (u32_t)id;

    if (xQueueSend(qThdSpv, (void *)&send_msg, IMMEDIATELY) != pdTRUE)
    {
        if (xQueueSend(qThdSpv, (void *)&send_msg, QSEND_WAIT_MS) != pdTRUE)
        {
#ifdef DEBUG_ME
            printf("Error: xQueueSend qThdSpv Failed!!!\r\n");
#endif
            return pdFAIL;
        }
    }

    /*TODO: must be handle if failed terminate thread*/

    return pdPASS;
}

u8_t CreateListenPortThd(struct netconn *obj_listen_port)
{
    Message recv_msg, send_msg;
    u32_t cmd, result;

    send_msg.payload_0 = CREATE_THD_LISTEN_PORT;
    send_msg.payload_1 = (u32_t)obj_listen_port;

    if (xQueueSend(qThdSpv, (void *)&send_msg, IMMEDIATELY) != pdTRUE)
    {
        if (xQueueSend(qThdSpv, (void *)&send_msg, QSEND_WAIT_MS) != pdTRUE)
        {
#ifdef DEBUG_ME
            printf("Error: xQueueSend qThdSpv Failed!!!\r\n");
#endif
            return pdFAIL;
        }
    }

    /*TODO: Should be handled if timeout*/
    if (xQueueReceive(qReplyThdSpvLport, (void *)&recv_msg, osWaitForever) != pdTRUE)
        return pdFAIL;

    cmd = recv_msg.payload_0;
    result = recv_msg.payload_2;

    // the object that send & recv must be same
    if (cmd != REPLY_CREATE_THD_LISTEN_PORT)
        return pdFAIL;

    if (send_msg.payload_1 != recv_msg.payload_1)
        return pdFAIL;

    if (result != pdPASS)
        return pdFAIL;

    return pdPASS;
}

u8_t CreateClientThread(struct netconn *new_netconn)
{
    Message recv_msg, send_msg;
    u32_t cmd, result;

    send_msg.payload_0 = CREATE_THD_CLIENT;
    send_msg.payload_1 = (u32_t)new_netconn;

    if (xQueueSend(qThdSpv, (void *)&send_msg, IMMEDIATELY) != pdTRUE)
    {
        if (xQueueSend(qThdSpv, (void *)&send_msg, QSEND_WAIT_MS) != pdTRUE)
        {
#ifdef DEBUG_ME
            printf("Error: xQueueSend qThdSpv Failed!!!\r\n");
#endif
            return pdFAIL;
        }
    }

    if (xQueueReceive(qReplyThdSpvClient, (void *)&recv_msg, osWaitForever) != pdTRUE)
        return pdFAIL;

    cmd = recv_msg.payload_0;
    result = recv_msg.payload_2;

    // the object that send & recv must be same
    if (cmd != REPLY_CREATE_THD_CLIENT)
        return pdFAIL;

    if (send_msg.payload_1 != recv_msg.payload_1)
        return pdFAIL;

    if (result != pdPASS)
        return pdFAIL;

    return pdPASS;
}

void spv_thd(void *parameters)
{
    Message recv_msg, send_msg;
    u32_t cmd, passing_param, result, index;
    osThreadId manage_thd_id, notify_thd_id;
    osStatus status;
    char name_thd[configMAX_TASK_NAME_LEN];
    struct netconn *delete_netconn;
    cfg_dat_t *this_cfg;
    EventBits_t tcpClientBit, lPortThdBit;
    QueueHandle_t reply;
    struct tcp_pcb *tcp;
    u16_t listen_port;
    err_t statusNet;

    notify_thd_id = (osThreadId)parameters;
    this_cfg = get_config_ptr();

    /* Send notification to task_who_created(), bringing it out of the Blocked state. */
    xTaskNotifyGive(notify_thd_id);
    /* used in another function */

    while (1)
    {
        osDelay(1);

        if (xQueueReceive(qThdSpv, (void *)&recv_msg, osWaitForever) != pdTRUE)
            continue;

        cmd = recv_msg.payload_0;
        switch (cmd)
        {
        case CREATE_THD_LISTEN_PORT:
            passing_param = recv_msg.payload_1;

            // set indicator of bit client created
            lPortThdBit = xEventGroupGetBits(lPortThdEvGroup);
            index = check_bit_status_empty(lPortThdBit);
            if (CHECK_BIT(lPortThdBit, index) != 0)
                result = pdFAIL;
            else
            {
                // this is indication of all max thread is active
                if (index != 255)
                {
#ifdef DEBUG_ME
                    printf("create thd listen port (obj 0x%x) %d [%d]\r\n",
                           passing_param, this_cfg->listen_port[index], index);
#endif

                    sprintf(name_thd, "lport_thd_%d", index);
                    if (sys_thread_new(name_thd,
                                       (lwip_thread_fn)funcListenPortThd,
                                       (void *)passing_param,
                                       DEFAULT_THREAD_STACKSIZE,
                                       osPriorityAboveNormal) == NULL)
                    {
                        result = pdFAIL;
                    }
                    else
                    {
                        result = pdPASS;
                    }

                    // wait make sure that success run to while loop netconn receive
                    lPortThdBit = xEventGroupWaitBits(
                        lPortThdEvGroup,
                        (1 << index),
                        pdFALSE,
                        pdFALSE,
                        osWaitForever);
                }
                else
                {
                    result = pdFAIL;
                }
            }

            send_msg.payload_0 = REPLY_CREATE_THD_LISTEN_PORT;
            send_msg.payload_1 = passing_param;
            send_msg.payload_2 = result;

            break;
        case CREATE_THD_CLIENT:
            passing_param = recv_msg.payload_1;

            tcpClientBit = xEventGroupGetBits(tcpClientEvGroup);
            index = check_bit_status_empty(tcpClientBit);
            if (CHECK_BIT(tcpClientBit, index) != 0)
            {
                result = pdFAIL;
            }
            else
            {
                index = check_bit_status_empty(tcpClientBit);
                if (index == 255)
                {
                    result = pdFAIL;
                }
                else
                {
                    sprintf(name_thd, "client_thd_%d", index);
                    if (sys_thread_new(name_thd,
                                       (lwip_thread_fn)funcClientThd,
                                       (void *)passing_param,
                                       DEFAULT_THREAD_STACKSIZE * 3,
                                       osPriorityNormal) == NULL)
                    {
                        result = pdFAIL;
                    }
                    else
                    {
                        result = pdPASS;
                    }

                    // wait make sure that success run to while loop netconn receive
                    tcpClientBit = xEventGroupWaitBits(
                        tcpClientEvGroup,
                        (1 << index),
                        pdFALSE,
                        pdFALSE,
                        osWaitForever);
                }
            }

            send_msg.payload_0 = REPLY_CREATE_THD_CLIENT;
            send_msg.payload_1 = passing_param;
            send_msg.payload_2 = result;

            break;
        case CLOSE_THD_CLIENT:
            delete_netconn = (struct netconn *)recv_msg.payload_1;
            manage_thd_id = (osThreadId)recv_msg.payload_2;

            tcp = delete_netconn->pcb.tcp;
            tcp_tcp_get_tcp_addrinfo(tcp, 1, NULL, &listen_port);
            if (listen_port == EVPU_PROTOCOL_PORT)
                clear_obj_evpu_tcp();

            /* Close connection and discard connection identifier. */
            statusNet = netconn_close(delete_netconn);
            if (statusNet == ERR_OK)
            {
                statusNet = netconn_delete(delete_netconn);
                if (statusNet == ERR_OK)
                {
                    printf("CLOSE => %s\r\n", pcTaskGetName(manage_thd_id));
                    status = osThreadTerminate(manage_thd_id);
                    if (status == osOK)
                        result = pdPASS;
                    else
                        result = pdFAIL;
                }
                else
                {
                    result = pdFAIL;
                }
            }
            else
            {
                result = pdFAIL;
            }

            index = get_idx_with_compare(delete_netconn);
            if (index != 255)
            {
                tcp_conn[index] = NULL;
            }
            else
            {
                result = pdFAIL;
            }

            // must be handle if terminate thread fail
            send_msg.payload_0 = REPLY_CLOSE_THD_CLIENT;
            send_msg.payload_1 = passing_param;
            send_msg.payload_2 = result;

            break;
        default:
            break;
        }

        // dont reply bcz thread consumer has been terminated
        if (cmd == CLOSE_THD_CLIENT)
        {
            if (result == pdPASS)
            {
                continue;
            }
            else
            {
#ifdef DEBUG_ME
                printf("ERR: osThreadTerminate Failed!!!\r\n");
#endif
            }
        }

        if (cmd == CLOSE_THD_CLIENT || cmd == CREATE_THD_CLIENT)
            reply = qReplyThdSpvClient;
        else if (cmd == CREATE_THD_LISTEN_PORT || cmd == CLOSE_THD_LISTEN_PORT)
            reply = qReplyThdSpvLport;

        if (reply == NULL)
            continue;

        // thread consumer waiting until done and send result
        if (xQueueSend(reply, (void *)&send_msg, IMMEDIATELY) != pdTRUE)
        {
            if (xQueueSend(reply, (void *)&send_msg, QSEND_WAIT_MS) != pdTRUE)
            {
#ifdef DEBUG_ME
                printf("Error: xQueueSend qReplyThdSpvClient failed!!!\r\n");
#endif
            }
        }

#if 0
        task_list(NULL,NULL);
#endif
    }
}

u8_t create_supervision_thd(osThreadId thd_created, void *fn_client, void *fn_listenPort)
{
    if (fn_client == NULL || fn_listenPort == NULL)
        return pdFAIL;

    funcClientThd = fn_client;
    funcListenPortThd = fn_listenPort;

    qThdSpv = xQueueCreate(qCmdProcess_LEN, sizeof(Message));
    if (qThdSpv == NULL)
        return pdFAIL;

    qReplyThdSpvLport = xQueueCreate(qCmdProcess_LEN, sizeof(Message));
    if (qReplyThdSpvLport == NULL)
        return pdFAIL;

    qReplyThdSpvClient = xQueueCreate(qCmdProcess_LEN, sizeof(Message));
    if (qReplyThdSpvClient == NULL)
        return pdFAIL;

    if (sys_thread_new("spv_thread",
                       spv_thd,
                       (void *)thd_created,
                       DEFAULT_THREAD_STACKSIZE,
                       osPriorityNormal) == NULL)
    {
        return pdFAIL;
    }

    return pdPASS;
}
