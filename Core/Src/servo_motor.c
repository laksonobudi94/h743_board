/*-----------------------------------------------------------
 * DMM Technology Corp.
 *
 * DYN AC Servo Drive CAN Specification
 * [DYNCAN1-BL314-12A]
 *
 * Document Version 1.2A
 * Published March 20, 2018
 *
 * See https://www.youtube.com/watch?v=-bydTRYfSHs&feature=youtu.be
 *----------------------------------------------------------*/
#include "servo_motor.h"

void Servo_CMD_Init(FDCAN_TxHeaderTypeDef *tx_header)
{
    tx_header->Identifier = SERVO_DRIVE_ID << 5;
    tx_header->IdType = FDCAN_STANDARD_ID;
    tx_header->TxFrameType = FDCAN_DATA_FRAME;
    tx_header->DataLength = FDCAN_DLC_BYTES_8;
    tx_header->ErrorStateIndicator = FDCAN_ESI_ACTIVE;
    tx_header->BitRateSwitch = FDCAN_BRS_OFF;
    tx_header->FDFormat = FDCAN_CLASSIC_CAN;
    tx_header->TxEventFifoControl = FDCAN_NO_TX_EVENTS;
    tx_header->MessageMarker = 0;
}

u8_t Servo_CMD_Set_Origin(FDCAN_TxHeaderTypeDef *tx_header, u8_t *data)
{
    /*
    Sending data 0xFF sets the current motor position as the absolute zero position.
    When the drive is set to operate in Absolute Mode (Configuration&0x04=1),
    when the drive powers ON, the motor moves to the set absolute zero position,
    then starts accepting command.
    */

    tx_header->Identifier |= (Set_Origin & MASK_5_BYTE);
    data[0] = 0xFF;
    tx_header->DataLength = FDCAN_DLC_BYTES_1;

    return pdPASS;
}

u8_t Servo_CMD_Go_Absolute_Pos_PTP(FDCAN_TxHeaderTypeDef *tx_header, s32_t abs_pos, u8_t *data)
{
    /*
    Point to Point Move Absolute Position command.
    Positive command turns motor in CW direction, negative command turns motor in CCW direction.
    Drive runs motion as soon as command received.
    Point to Point command acceleration/deceleration determined by Max Acceleration parameter.
    Speed determined by Max Speed Parameter.
    Servo drive return CAN Data Error if command position outside allowed data range.
    */

    if (abs_pos <= MIN_RANGE_POS)
        abs_pos = MIN_RANGE_POS;
    if (abs_pos >= MAX_RANGE_POS)
        abs_pos = MAX_RANGE_POS;

    tx_header->Identifier |= (Go_Absolute_Pos_PTP & MASK_5_BYTE);
    data[0] = abs_pos & 0x000000FF;
    data[1] = (abs_pos & 0x0000FF00) >> 8;
    data[2] = (abs_pos & 0x00FF0000) >> 16;
    data[3] = (abs_pos & 0xFF000000) >> 24;

    if (abs_pos <= SCHAR_MAX && abs_pos >= SCHAR_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_1;
    else if (abs_pos <= SHRT_MAX && abs_pos >= SHRT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_2;
    else if (abs_pos <= 0x7fffff && abs_pos >= (~0x7fffff)) // 3 byte signed data type range
        tx_header->DataLength = FDCAN_DLC_BYTES_3;
    else if (abs_pos <= INT_MAX && abs_pos >= INT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_4;
    else
        tx_header->DataLength = FDCAN_DLC_BYTES_0;

    if (tx_header->DataLength == FDCAN_DLC_BYTES_0)
        return pdFAIL;

    return pdPASS;
}

u8_t Servo_CMD_Make_LinearLine(FDCAN_TxHeaderTypeDef *tx_header, s32_t coordinate, u8_t *data)
{
    /*
    Up to 3 axis coordinated linear line command.
    Drive runs motion as soon as command received.
    Servo drive return CAN Data Error if command position outside allowed data range
    */

    if (coordinate <= MIN_RANGE_POS)
        coordinate = MIN_RANGE_POS;
    if (coordinate >= MAX_RANGE_POS)
        coordinate = MAX_RANGE_POS;

    tx_header->Identifier |= (Make_LinearLine & MASK_5_BYTE);
    data[0] = coordinate & 0x000000FF;
    data[1] = (coordinate & 0x0000FF00) >> 8;
    data[2] = (coordinate & 0x00FF0000) >> 16;
    data[3] = (coordinate & 0xFF000000) >> 24;

    if (coordinate <= SCHAR_MAX && coordinate >= SCHAR_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_1;
    else if (coordinate <= SHRT_MAX && coordinate >= SHRT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_2;
    else if (coordinate <= 0x7fffff && coordinate >= (~0x7fffff)) // 3 byte signed data type range
        tx_header->DataLength = FDCAN_DLC_BYTES_3;
    else if (coordinate <= INT_MAX && coordinate >= INT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_4;
    else
        tx_header->DataLength = FDCAN_DLC_BYTES_0;

    if (tx_header->DataLength == FDCAN_DLC_BYTES_0)
        return pdFAIL;

    return pdPASS;
}

u8_t Servo_CMD_Go_Relative_Pos_PTP(FDCAN_TxHeaderTypeDef *tx_header, s32_t rel_pos, u8_t *data)
{
    /*
    Point to Point Move Relative Position command.
    Positive command turns motor in CW direction, negative command turns motor in CCW direction.
    Drive runs motion as soon as command received.
    Point to Point command acceleration/deceleration determined by Max Acceleration parameter.
    Speed determined by Max Speed Parameter.
    Servo drive return CAN Data Error if command position outside allowed data range
    */

    if (rel_pos <= MIN_RANGE_POS)
        rel_pos = MIN_RANGE_POS;
    if (rel_pos >= MAX_RANGE_POS)
        rel_pos = MAX_RANGE_POS;

    tx_header->Identifier |= (Go_Relative_Pos_PTP & MASK_5_BYTE);
    data[0] = rel_pos & 0x000000FF;
    data[1] = (rel_pos & 0x0000FF00) >> 8;
    data[2] = (rel_pos & 0x00FF0000) >> 16;
    data[3] = (rel_pos & 0xFF000000) >> 24;

    if (rel_pos <= SCHAR_MAX && rel_pos >= SCHAR_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_1;
    else if (rel_pos <= SHRT_MAX && rel_pos >= SHRT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_2;
    else if (rel_pos <= 0x7fffff && rel_pos >= (~0x7fffff)) // 3 byte signed data type range
        tx_header->DataLength = FDCAN_DLC_BYTES_3;
    else if (rel_pos <= INT_MAX && rel_pos >= INT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_4;
    else
        tx_header->DataLength = FDCAN_DLC_BYTES_0;

    if (tx_header->DataLength == FDCAN_DLC_BYTES_0)
        return pdFAIL;

    return pdPASS;
}

u8_t Servo_CMD_Make_CircularArc(FDCAN_TxHeaderTypeDef *tx_header, s32_t coordinate, u8_t *data)
{
    /*
    Up to 3 axis coordinated linear line command.
    Drive runs motion as soon as command received.
    Servo drive return CAN Data Error if command position outside allowed data range
    */

    if (coordinate <= MIN_RANGE_POS)
        coordinate = MIN_RANGE_POS;
    if (coordinate >= MAX_RANGE_POS)
        coordinate = MAX_RANGE_POS;

    tx_header->Identifier |= (Make_CircularArc & MASK_5_BYTE);
    data[0] = coordinate & 0x000000FF;
    data[1] = (coordinate & 0x0000FF00) >> 8;
    data[2] = (coordinate & 0x00FF0000) >> 16;
    data[3] = (coordinate & 0xFF000000) >> 24;

    if (coordinate <= SCHAR_MAX && coordinate >= SCHAR_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_1;
    else if (coordinate <= SHRT_MAX && coordinate >= SHRT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_2;
    else if (coordinate <= 0x7fffff && coordinate >= (~0x7fffff)) // 3 byte signed data type range
        tx_header->DataLength = FDCAN_DLC_BYTES_3;
    else if (coordinate <= INT_MAX && coordinate >= INT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_4;
    else
        tx_header->DataLength = FDCAN_DLC_BYTES_0;

    if (tx_header->DataLength == FDCAN_DLC_BYTES_0)
        return pdFAIL;

    return pdPASS;
}

u8_t Servo_CMD_Assign_Drive_ID(FDCAN_TxHeaderTypeDef *tx_header, u8_t drive_id, u8_t *data)
{
    /*
    Assign or Read servo drive ID number.
    When assigning new ID number, wait 10ms for servo drive to re-initialize CAN port with new ID number.
    Note since the 11-bit Identifier contains both the Function Code and Drive ID,
    the 11-bit identifier must also be changed after the Drive ID changes.
    Servo drive return CAN Data Error if assigned ID is not 0~64.
    */

    if (drive_id >= 64)
        drive_id = 64;

    data[0] = drive_id;

    tx_header->Identifier |= (Assign_Drive_ID & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_1;
    return pdPASS;
}

u8_t Servo_CMD_Read_Drive_ID(FDCAN_TxHeaderTypeDef *tx_header)
{
    /*
    Assign or Read servo drive ID number.
    When assigning new ID number, wait 10ms for servo drive to re-initialize CAN port with new ID number.
    Note since the 11-bit Identifier contains both the Function Code and Drive ID,
    the 11-bit identifier must also be changed after the Drive ID changes.
    Servo drive return CAN Data Error if assigned ID is not 0~64.
    */

    tx_header->Identifier |= (Read_Drive_ID & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_0;
    return pdPASS;
}

u8_t Servo_CMD_Set_Drive_Config(FDCAN_TxHeaderTypeDef *tx_header, drive_cfg_t drive_cfg, u8_t *data)
{
    /*Set and read servo drive configuration byte.*/

    tx_header->Identifier |= (Set_Drive_Config & MASK_5_BYTE);
    data[0] = drive_cfg.byte;
    tx_header->DataLength = FDCAN_DLC_BYTES_1;
    return pdPASS;
}

u8_t Servo_CMD_Read_Drive_Config(FDCAN_TxHeaderTypeDef *tx_header)
{
    /*Set and read servo drive configuration byte.*/

    tx_header->Identifier |= (Read_Drive_Config & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_0;
    return pdPASS;
}

u8_t Servo_CMD_Read_Drive_Status(FDCAN_TxHeaderTypeDef *tx_header)
{
    /*Read the servo drive Status.*/

    tx_header->Identifier |= (Read_Drive_Status & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_0;
    return pdPASS;
}

u8_t Servo_CMD_Turn_ConstSpeed(FDCAN_TxHeaderTypeDef *tx_header, s16_t speed, u8_t *data)
{
    /*
    Turn motor at constant speed.
    Sending command immediately starts motor rotation.
    Max Acceleration parameter controls acceleration when changing speed.
    Positive command turns motor in CW direction, negative command turns motor in CCW direction.
    Servo drive return CAN Data Error if speed command is not within -20,000~20,000.
    */

    if (speed <= MIN_RANGE_SPEED)
        speed = MIN_RANGE_SPEED;
    if (speed >= MAX_RANGE_SPEED)
        speed = MAX_RANGE_SPEED;

    tx_header->Identifier |= (Turn_ConstSpeed & MASK_5_BYTE);
    data[0] = speed & 0x000000FF;
    data[1] = (speed & 0x0000FF00) >> 8;

    if (speed <= SCHAR_MAX && speed >= SCHAR_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_1;
    else if (speed <= SHRT_MAX && speed >= SHRT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_2;
    else
        tx_header->DataLength = FDCAN_DLC_BYTES_0;

    if (tx_header->DataLength == FDCAN_DLC_BYTES_0)
        return pdFAIL;

    return pdPASS;
}

u8_t Servo_CMD_Square_Wave(FDCAN_TxHeaderTypeDef *tx_header, s32_t amplitude, u8_t *data)
{
    /*
    Sets built-in Square Wave motion amplitude.
    1024 = 45degree, 2048 = 90degree, 4096 = 180degree amplitude.
    Motion begins when register is command is given,
    and SS_Frequency command is not zero.
    Servo drive return CAN Data Error if sent data not within range.
    */

    if (amplitude <= MIN_RANGE_WAVE)
        amplitude = MIN_RANGE_WAVE;
    if (amplitude >= MAX_RANGE_WAVE)
        amplitude = MAX_RANGE_WAVE;

    tx_header->Identifier |= (Square_Wave & MASK_5_BYTE);

    data[0] = amplitude & 0x000000FF;
    data[1] = (amplitude & 0x0000FF00) >> 8;
    data[2] = (amplitude & 0x00FF0000) >> 16;

    if (amplitude <= SCHAR_MAX && amplitude >= SCHAR_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_1;
    else if (amplitude <= SHRT_MAX && amplitude >= SHRT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_2;
    else if (amplitude <= 0x7fffff && amplitude >= (~0x7fffff)) // 3 byte signed data type range
        tx_header->DataLength = FDCAN_DLC_BYTES_3;
    else
        tx_header->DataLength = FDCAN_DLC_BYTES_0;

    if (tx_header->DataLength == FDCAN_DLC_BYTES_0)
        return pdFAIL;

    return pdPASS;
}

u8_t Servo_CMD_Sin_Wave(FDCAN_TxHeaderTypeDef *tx_header, s32_t amplitude, u8_t *data)
{
    /*
    Sets built-in Square Wave motion amplitude.
    1024 = 45degree, 2048 = 90degree, 4096 = 180degree amplitude.
    Motion begins when register is command is given,
    and SS_Frequency command is not zero.
    Servo drive return CAN Data Error if sent data not within range.
    */

    if (amplitude <= MIN_RANGE_WAVE)
        amplitude = MIN_RANGE_WAVE;
    if (amplitude >= MAX_RANGE_WAVE)
        amplitude = MAX_RANGE_WAVE;

    tx_header->Identifier |= (Sin_Wave & MASK_5_BYTE);

    data[0] = amplitude & 0x000000FF;
    data[1] = (amplitude & 0x0000FF00) >> 8;
    data[2] = (amplitude & 0x00FF0000) >> 16;

    if (amplitude <= SCHAR_MAX && amplitude >= SCHAR_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_1;
    else if (amplitude <= SHRT_MAX && amplitude >= SHRT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_2;
    else if (amplitude <= 0x7fffff && amplitude >= (~0x7fffff)) // 3 byte signed data type range
        tx_header->DataLength = FDCAN_DLC_BYTES_3;
    else
        tx_header->DataLength = FDCAN_DLC_BYTES_0;

    if (tx_header->DataLength == FDCAN_DLC_BYTES_0)
        return pdFAIL;

    return pdPASS;
}

u8_t Servo_CMD_SS_Frequency(FDCAN_TxHeaderTypeDef *tx_header, u8_t freq, u8_t *data)
{
    /*
    Sets built-in Square/Sine Wave motion frequency. Units in Hertz.
    Setting resets to zero when drive powered OFF.
    Servo drive return CAN Data Error if sent data not within range.
    */

    tx_header->Identifier |= (SS_Frequency & MASK_5_BYTE);
    data[0] = freq < MAX_FREQ ? freq : MAX_FREQ;
    tx_header->DataLength = FDCAN_DLC_BYTES_1;
    return pdPASS;
}

u8_t Servo_CMD_General_Read(FDCAN_TxHeaderTypeDef *tx_header, u8_t *data)
{
    /*
    Read 32-bit motor position or torque command.

    To read motor 32-bit absolute encoder position, send 0x0e function code with 1 byte data 0x1b.
    Drive will respond with 32-bit signed absolute encoder position.
    Data length depends on position decimal value �C see Data Length and Data Field in section 3.
    Absolute position is positive in CW direction, negative in CCW direction.

    To read motor torque, send 0x03 function code with 1 byte data 0x1e.
    Drive will respond with 1~2byte torque value.
    Torque  value range is [-700:+700].
    700=peak output current of servo drive.
    Value is positive when current/torque is applied in CCW direction.

    */

    tx_header->Identifier |= (General_Read & MASK_5_BYTE);
    data[0] = 0x1b;
    tx_header->DataLength = FDCAN_DLC_BYTES_1;
    return pdPASS;
}

u8_t Servo_CMD_Set_Param_Group1(FDCAN_TxHeaderTypeDef *tx_header, set_param_group1_t set_param1, u8_t *data)
{
    /*
    Sets Parameter Group1 including Main Gain, Speed Gain, Integration Gain and Torque Filter Constant.
    Send 0x10 function code with 4 byte data.
    First byte Main Gain, second byte Speed Gain, third byte Integration Gain, fourth byte  Torque Filter Constant.
    Sending 0x00 data byte does not save that parameter.
    All parameter has setting range [1:127].
    Servo drive return CAN Data Error if sent data not within range.
    */

    if (set_param1.Main_Gain < MIN_GAIN)
        set_param1.Main_Gain = MIN_GAIN;
    else if (set_param1.Main_Gain > MAX_GAIN)
        set_param1.Main_Gain = MAX_GAIN;

    if (set_param1.Speed_Gain < MIN_GAIN)
        set_param1.Speed_Gain = MIN_GAIN;
    else if (set_param1.Speed_Gain > MAX_GAIN)
        set_param1.Speed_Gain = MAX_GAIN;

    if (set_param1.Integration_Gain < MIN_GAIN)
        set_param1.Integration_Gain = MIN_GAIN;
    else if (set_param1.Integration_Gain > MAX_GAIN)
        set_param1.Integration_Gain = MAX_GAIN;

    if (set_param1.Torque_Filter_Constant < MIN_GAIN)
        set_param1.Torque_Filter_Constant = MIN_GAIN;
    else if (set_param1.Torque_Filter_Constant > MAX_GAIN)
        set_param1.Torque_Filter_Constant = MAX_GAIN;

    tx_header->Identifier |= (Set_Param_Group1 & MASK_5_BYTE);

    data[0] = set_param1.Main_Gain;
    data[1] = set_param1.Speed_Gain;
    data[2] = set_param1.Integration_Gain;
    data[3] = set_param1.Torque_Filter_Constant;

    tx_header->DataLength = FDCAN_DLC_BYTES_4;

    return pdPASS;
}

u8_t Servo_CMD_Set_Param_Group2(FDCAN_TxHeaderTypeDef *tx_header, set_param_group2_t set_param2, u8_t *data)
{
    /*
    Sets Parameter Group 2 including Max Speed, Max Acceleration.
    Max Speed and Max Acceleration saves into EEPROM first 10 times after power ON.
    After saves into RAM and is erased when drive powered OFF.
    Send 0x11 function code with 2 byte data.
    First byte Max Speed, second byte Max Acceleration.
    Sending 0x00 data byte does not save that parameter.
    All parameter has setting range [1:127].
    Servo drive return CAN Data Error if sent data not within range.
    */

    if (set_param2.Max_Speed < MIN_GAIN)
        set_param2.Max_Speed = MIN_GAIN;
    else if (set_param2.Max_Speed > MAX_GAIN)
        set_param2.Max_Speed = MAX_GAIN;

    if (set_param2.Max_Acceleration < MIN_GAIN)
        set_param2.Max_Acceleration = MIN_GAIN;
    else if (set_param2.Max_Acceleration > MAX_GAIN)
        set_param2.Max_Acceleration = MAX_GAIN;

    tx_header->Identifier |= (Set_Param_Group2 & MASK_5_BYTE);

    data[0] = set_param2.Max_Speed;
    data[1] = set_param2.Max_Acceleration;

    tx_header->DataLength = FDCAN_DLC_BYTES_2;

    return pdPASS;
}

u8_t Servo_CMD_Set_Param_Group3(FDCAN_TxHeaderTypeDef *tx_header, set_param_group3_t set_param3, u8_t *data)
{
    /*
    Sets Parameter Group1 including On Pos Rage, GEAR_NUMBER (2 bytes), and LINE_NUMBER (2 bytes).
    Send 0x12 function code with 5 byte data.
    First byte On Pos Range, second and third byte is GEAR_NUMER, fourth and fifth byte is LINE_NUMER.
    Sending 0x00 data byte does not save that parameter.
    On Pos Range parameter has setting range [1:127].
    GEAR_NUMBER has setting range [500:16384].
    LINE_NUMER has setting range [500:4095].
    Servo drive return CAN Data Error if sent data not within range.
    */

    if (set_param3.On_Pos_Rage < MIN_GAIN)
        set_param3.On_Pos_Rage = MIN_GAIN;
    else if (set_param3.On_Pos_Rage > MAX_GAIN)
        set_param3.On_Pos_Rage = MAX_GAIN;

    if (set_param3.gear_number < MIN_GEAR_NUMBER)
        set_param3.gear_number = MIN_GEAR_NUMBER;
    else if (set_param3.gear_number > MAX_GEAR_NUMBER)
        set_param3.gear_number = MAX_GEAR_NUMBER;

    if (set_param3.line_number < MIN_LINE_NUMBER)
        set_param3.line_number = MIN_LINE_NUMBER;
    else if (set_param3.line_number > MAX_LINE_NUMBER)
        set_param3.line_number = MAX_LINE_NUMBER;

    tx_header->Identifier |= (Set_Param_Group3 & MASK_5_BYTE);

    data[0] = set_param3.On_Pos_Rage;
    data[1] = set_param3.gear_number & 0x00FF;
    data[2] = (set_param3.gear_number & 0xFF00) >> 8;
    data[3] = set_param3.line_number & 0x00FF;
    data[4] = (set_param3.line_number & 0xFF00) >> 8;

    tx_header->DataLength = FDCAN_DLC_BYTES_5;

    return pdPASS;
}

u8_t Servo_CMD_Read_Param_Group1(FDCAN_TxHeaderTypeDef *tx_header)
{
    /*
    Reads Parameter Group1.
    Servo drive returns 4 bytes.
    First byte Main Gain, second byte Speed Gain, third byte Integration Gain, fourth byte Torque Filter Constant.
    */

    tx_header->Identifier |= (Read_Param_Group1 & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_0;
    return pdPASS;
}

u8_t Servo_CMD_Read_Param_Group2(FDCAN_TxHeaderTypeDef *tx_header)
{
    /*
    Reads Parameter Group2.
    Servo drive returns 2 bytes.
    First byte Max Speed, second byte Max Acceleration.
    */

    tx_header->Identifier |= (Read_Param_Group2 & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_0;
    return pdPASS;
}

u8_t Servo_CMD_Read_Param_Group3(FDCAN_TxHeaderTypeDef *tx_header)
{
    /*
    Reads Parameter Group3.
    Servo drive returns 5 bytes.
    First byte On Pos Range, second and third byte is GEAR_NUMER, fourth and fifth byte is LINE_NUMER.
    */

    tx_header->Identifier |= (Read_Param_Group3 & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_0;
    return pdPASS;
}

u8_t Servo_CMD_Go_Absolute_Pos_Sync(FDCAN_TxHeaderTypeDef *tx_header, s32_t abs_pos, u8_t *data)
{
    /*
    Synchronized PTP Move Absolute Position command.
    Sending data loads Synchronized Absolute Position command.
    Servo drive does not run motor.
    Motion begins when Sync_Trigger (FC=0x18) and 0xFF data command received.
    Servo drive return CAN Data Error if sent data not within range
    */

    if (abs_pos <= MIN_RANGE_POS)
        abs_pos = MIN_RANGE_POS;
    if (abs_pos >= MAX_RANGE_POS)
        abs_pos = MAX_RANGE_POS;

    tx_header->Identifier |= (Go_Absolute_Pos_Sync & MASK_5_BYTE);
    data[0] = abs_pos & 0x000000FF;
    data[1] = (abs_pos & 0x0000FF00) >> 8;
    data[2] = (abs_pos & 0x00FF0000) >> 16;
    data[3] = (abs_pos & 0xFF000000) >> 24;

    if (abs_pos <= SCHAR_MAX && abs_pos >= SCHAR_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_1;
    else if (abs_pos <= SHRT_MAX && abs_pos >= SHRT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_2;
    else if (abs_pos <= 0x7fffff && abs_pos >= (~0x7fffff)) // 3 byte signed data type range
        tx_header->DataLength = FDCAN_DLC_BYTES_3;
    else if (abs_pos <= INT_MAX && abs_pos >= INT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_4;
    else
        tx_header->DataLength = FDCAN_DLC_BYTES_0;

    if (tx_header->DataLength == FDCAN_DLC_BYTES_0)
        return pdFAIL;

    return pdPASS;
}

u8_t Servo_CMD_Go_Relative_Pos_Sync(FDCAN_TxHeaderTypeDef *tx_header, s32_t rel_pos, u8_t *data)
{
    /*
    Synchronized PTP Move Relative Position command.
    Sending data loads Synchronized Relative Position command.
    Servo drive does not run motor.
    Motion begins when Sync_Trigger (FC=0x18) ) and 0xEE data command received.
    Servo drive return CAN Data Error if sent data not within range.
    */

    if (rel_pos <= MIN_RANGE_POS)
        rel_pos = MIN_RANGE_POS;
    if (rel_pos >= MAX_RANGE_POS)
        rel_pos = MAX_RANGE_POS;

    tx_header->Identifier |= (Go_Relative_Pos_Sync & MASK_5_BYTE);
    data[0] = rel_pos & 0x000000FF;
    data[1] = (rel_pos & 0x0000FF00) >> 8;
    data[2] = (rel_pos & 0x00FF0000) >> 16;
    data[3] = (rel_pos & 0xFF000000) >> 24;

    if (rel_pos <= SCHAR_MAX && rel_pos >= SCHAR_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_1;
    else if (rel_pos <= SHRT_MAX && rel_pos >= SHRT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_2;
    else if (rel_pos <= 0x7fffff && rel_pos >= (~0x7fffff)) // 3 byte signed data type range
        tx_header->DataLength = FDCAN_DLC_BYTES_3;
    else if (rel_pos <= INT_MAX && rel_pos >= INT_MIN)
        tx_header->DataLength = FDCAN_DLC_BYTES_4;
    else
        tx_header->DataLength = FDCAN_DLC_BYTES_0;

    if (tx_header->DataLength == FDCAN_DLC_BYTES_0)
        return pdFAIL;

    return pdPASS;
}

u8_t Servo_CMD_Sync_Trigger(FDCAN_TxHeaderTypeDef *tx_header, sync_trigger_enum_t sync_trigger, u8_t *data)
{
    /*
    Synchronized PTP Move start motion trigger.
    Sending 1 byte data 0xFF starts Go_Absolute_Pos_Sync motion with loaded position.
    Sending 1 byte data 0xEE starts Go_Relative_Pos_Sync motion with loaded position.
    For multi-axis applications, using Sync motion command allows better synchronization between each axis.
    First, load all axis with command.
    Then send all Sync_Trigger command to start motion.
    Use broadcast ID=0 to send Sync Trigger command to multiple axis simultaneously.
    */

    data[0] = (u8_t)sync_trigger;

    tx_header->Identifier |= (Sync_Trigger & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_1;
    return pdPASS;
}

u8_t Servo_CMD_Servo_Disable(FDCAN_TxHeaderTypeDef *tx_header, servo_activation_enum_t servo_activation, u8_t *data)
{
    /*
    Servo enable/disable command.
    Send 1 byte data 0x00 to enable servo.
    Send 1 byte data 0xFF to disable servo.
    */

    data[0] = (u8_t)servo_activation;

    tx_header->Identifier |= (Servo_Disable & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_1;
    return pdPASS;
}

u8_t Servo_CMD_Read_Motor_Speed(FDCAN_TxHeaderTypeDef *tx_header)
{
    /*Servo drive responds with current motor speed. Units in [rpm].*/

    tx_header->Identifier |= (Read_Motor_Speed & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_0;
    return pdPASS;
}

u8_t Servo_CMD_Go_Relative_Pos_PD(FDCAN_TxHeaderTypeDef *tx_header, s16_t pos_direct, u8_t *data)
{
    /*
    Send move relative position command directly to servo without PTP calculation.
    Servo drive runs command under max acceleration and speed.
    Position Direct (PD) command.
    Command is used for interpolation, CAM, and profile following commands
    where controller sends continuous position command to servo drive.
    */

    if (pos_direct < MIN_GEAR_NUMBER)
        pos_direct = MIN_GEAR_NUMBER;
    else if (pos_direct > MAX_GEAR_NUMBER)
        pos_direct = MAX_GEAR_NUMBER;

    tx_header->Identifier |= (Go_Relative_Pos_PD & MASK_5_BYTE);

    data[0] = pos_direct & 0x00FF;
    data[1] = (pos_direct & 0xFF00) >> 8;

    tx_header->DataLength = FDCAN_DLC_BYTES_2;

    return pdPASS;
}

u8_t Servo_CMD_Drive_Error(FDCAN_TxHeaderTypeDef *tx_header)
{
    /*
    CAN error message and servo drive Alarm output.
    If controller sends CAN message with invalid data range,
    servo drive responds with fc=0x1D and data = 0xFF to indicate data error.
    When servo drive Alarms, servo drive instantaneously sends CAN message with FC=0x1D and data = 0xEE to indicate alarmed state.
    Controller should monitor and stop operation when this message is received.
    */

    tx_header->Identifier |= (Drive_Error & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_0;
    return pdPASS;
}

u8_t Servo_CMD_Echo_Function(FDCAN_TxHeaderTypeDef *tx_header, u8_t *data)
{
    /*
    Echo function.
    Controller sends 4 byte data with FC=0x1E, Servo drive responds with same 4 byte data.
    */

    tx_header->Identifier |= (Echo_Function & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_4;
    return pdPASS;
}

u8_t Servo_CMD_Diagnostic_counter(FDCAN_TxHeaderTypeDef *tx_header)
{
    /*
    Drive internal unsigned 8-bit counter used for testing and diagnostics.
    Servo drive sends counter number when FC=0x1F command sent.
    Returned value increments by 1 each time it is read.
    Rolls back to 0 after 255.
    Can be used for CAN network management or heartbeat functionality
    */

    tx_header->Identifier |= (Diagnostic_counter & MASK_5_BYTE);
    tx_header->DataLength = FDCAN_DLC_BYTES_0;
    return pdPASS;
}
