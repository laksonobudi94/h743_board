#include "lwip/opt.h"

#if LWIP_NETCONN

#include "cmd_process.h"
#include "tcpecho.h"
#include "string.h"
#include "flash_data.h"
#include "evpu_protocol.h"
#include "cmsis_os.h"
#include "supervision.h"

u8_t TCPBuff[1528];

struct netconn *tcp_conn[MAX_TCP_CLIENT];
struct netconn *conn_port[MAX_TCP_PORT];

SemaphoreHandle_t bin_sem;

extern QueueHandle_t qThdSpv, qReplyThdSpvClient, qReplyThdSpvLport;

extern EventGroupHandle_t tcpClientEvGroup, lPortThdEvGroup;

void listen_port_thd(void *obj);
void tcp_client_thread(void *obj);

struct netconn *get_object_lwip(u8_t idx)
{
    return tcp_conn[idx];
}

/*TODO: All debug me must be change to enum error*/
static u8_t createServer(struct netconn **new_obj, u16_t port)
{
    err_t err;
    struct netconn *conn;

    /* Create a new connection identifier. */
    conn = netconn_new(NETCONN_TCP);
    if (conn == NULL)
        return pdFAIL;

    /* Bind connection to well known port number 7. */
    err = netconn_bind(conn, NULL, port);
    if (err != ERR_OK)
    {
        netconn_delete(conn);
        return pdFAIL;
    }

    /* Tell connection to go into listening mode. */
    err = netconn_listen(conn);
    if (err != ERR_OK)
        return pdFAIL;

    *new_obj = conn;
    return pdPASS;
}

void listen_port_thd(void *obj)
{
    err_t accept_err;
    struct netconn *new_connection;
    u8_t num_connect, no_tag;
    u16_t this_port;
    EventBits_t tcpClientBit, lPortThdBit;

    // get flag to find empty index for save a object pointer in array pointer
    lPortThdBit = xEventGroupGetBits(lPortThdEvGroup);
    no_tag = check_bit_status_empty(lPortThdBit);
    conn_port[no_tag] = (struct netconn *)obj;

    // set flag indicator thread successfully created
    lPortThdBit = xEventGroupSetBits(lPortThdEvGroup, (1 << no_tag));

    for (;;)
    {
        osDelay(1);

        /*TODO: Must be ask for closed thread*/
        if (conn_port[no_tag] == NULL)
            continue;
        if (new_connection != NULL)
            continue;

        /* Grab new connection. */
        // one more thread will be wait, but it has difference object
        accept_err = netconn_accept(conn_port[no_tag], &new_connection);
        if (accept_err != ERR_OK)
        {
            new_connection = NULL;
            continue;
        }

        tcpClientBit = xEventGroupGetBits(tcpClientEvGroup);
        num_connect = count_num_of_flag_set(tcpClientBit);
        tcp_tcp_get_tcp_addrinfo(conn_port[no_tag]->pcb.tcp,
                                 1,
                                 NULL,
                                 &this_port);

#ifdef DEBUG_ME
        printf("\r\n\r\nGrab new connection client num %d [Port %d]...\r\n",
               num_connect, this_port);
#endif

        // be carefull in priority every client have different priority
        // we are limit just 2 client can connect
        if (num_connect <= MAX_TCP_CLIENT)
        {
            // fail try again
            if (CreateClientThread(new_connection) != pdPASS)
                CreateClientThread(new_connection);
        }
        else
        {
            if (new_connection == NULL)
                continue;

            netconn_close(new_connection);
            netconn_delete(new_connection);
        }
        new_connection = NULL;
    }
}

void tcp_client_thread(void *obj)
{
    struct netbuf *buf;
    void *data;
    u16_t len, source_port;
    u8_t reply, no_tag, idx;
    err_t status, errval;
    EventBits_t tcpClientBit;
    osThreadId thd_id;
    ip_addr_t addr;
    SemaphoreHandle_t semETH;

    semETH = get_sem_obj_ETH();
    if (semETH == NULL)
        return;

    thd_id = osThreadGetId();
    if (thd_id == NULL)
        return;

    // set indicator of bit client created
    tcpClientBit = xEventGroupGetBits(tcpClientEvGroup);
    no_tag = check_bit_status_empty(tcpClientBit);
    if (no_tag == 255)
    {
        CloseClientThread(thd_id, tcp_conn[no_tag]);
        // just block wait thread terminated
        while (1)
            osDelay(1);
    }

    // keep in different location of address obj
    tcp_conn[no_tag] = (struct netconn *)obj;

    if (create_queue_reply(no_tag) == NULL)
    {
        CloseClientThread(thd_id, tcp_conn[no_tag]);
        // just block wait thread terminated
        while (1)
            osDelay(1);
    }

    // get source port of client remote
    tcp_tcp_get_tcp_addrinfo(tcp_conn[no_tag]->pcb.tcp,
                             0,
                             &addr,
                             &source_port);
    set_identity_client(source_port, no_tag);

    // set flag create thd success
    tcpClientBit = xEventGroupSetBits(tcpClientEvGroup, (1 << no_tag));

#ifdef DEBUG_ME
    printf("Create new connection TCP client [%d] => %s:%d\r\n",
           no_tag,
           ipaddr_ntoa(&addr),
           source_port);
#endif

    get_idx_queue_reply(source_port, &idx);

    // LWIP_SO_RCVTIMEO==1
    // dont be too fast it will problem duplicate ACK or re-transmition
    // netconn_set_recvtimeout(tcp_conn[no_tag], 100);
    tcp_conn[no_tag]->recv_timeout = 100;
    HeapStats_t thisHeap;

    while (1)
    {
        osDelay(1);
#if 0
            // make sure object not same in every connection
            printf("netconn_recv obj_tcp [%d]\r\n\
tcp_conn[0] 0x%x tcp_conn[0]->pcb.tcp=0x%x\r\n\
tcp_conn[1] 0x%x tcp_conn[1]->pcb.tcp=0x%x\r\n",
                   idx,
                   tcp_conn[0], tcp_conn[0]->pcb.tcp,
                   tcp_conn[1], tcp_conn[1]->pcb.tcp);
#endif

        // vPortGetHeapStats(&thisHeap);
        // printf("xAvailableHeapSpaceInBytes -> %d byte\r\n", thisHeap.xAvailableHeapSpaceInBytes);

        if (tcp_conn[no_tag] == NULL)
            continue;

        errval = netconn_recv(tcp_conn[no_tag], &buf);
        switch (errval)
        {
        case ERR_OK:
            // to avoid overflow mem when burst cmd
            if (netbuf_data(buf, &data, &len) != ERR_OK)
                break;

            status = DoCmdProcess(get_queue_reply(no_tag),
                                  source_port,
                                  data,
                                  &len,
                                  &reply);
            // printf("DoCmdProcess -> status %d (reply = %d len %d)\r\n", status,reply,len);

            netbuf_delete(buf);

            if ((status != ERR_OK) | (reply != pdTRUE) | (len == 0))
                break;

            if (write_netconn_sem(tcp_conn[no_tag], (u8_t *)data, len) != ERR_OK)
                printf("Err: write_netconn_sem Fail!\r\n");

            break;
        case ERR_TIMEOUT:
            break;
        case ERR_CLSD:
        case ERR_CONN:
            free_queue(get_queue_reply(no_tag));
            // clear flag of thread indicator that not active again
            // the tag can use in another of new connection object
            tcpClientBit = xEventGroupClearBits(tcpClientEvGroup, (1 << no_tag));

            /*TODO : becarefull if fail in here*/
            if (clear_identity_client(source_port) != pdPASS)
                break;

            CloseClientThread(thd_id, tcp_conn[no_tag]);
            // just block wait thread terminated
            while (1)
                osDelay(1);

            // should be never happen in here
        case ERR_WOULDBLOCK:
        default:
            printf("errval in netconn_recv -> %d\r\n", errval);
            break;
        }

        // if ok will be delete in qCmdProcess in queue after processed
        if (errval != ERR_OK && buf != NULL)
        {
            printf("netbuf_delete\r\n");
            netbuf_delete(buf);
        }
    }
}

tcp_server_init_err_t tcp_server_init(void)
{
    struct netconn *new_obj_port;
    cfg_dat_t *own_cfg;
    osThreadId this_thread_id;
    EventBits_t lPortThdBit;

    this_thread_id = osThreadGetId();
    if (this_thread_id == NULL)
        return GET_THD_ID_ERR;

    tcpClientEvGroup = xEventGroupCreate();
    if (tcpClientEvGroup == NULL)
        return CREATE_TCP_CLIENT_EVENT_ERR;

    lPortThdEvGroup = xEventGroupCreate();
    if (lPortThdEvGroup == NULL)
        return CREATE_LPORT_EVENT_ERR;

    init_evpu_err_t status = init_evpu();
    if (status != INIT_EVPU_SUCCESS)
    {
        printf("ERR: init_evpu FAIL [%d] !\r\n", status);
        return INIT_EVPU_ERR;
    }

    if (cmd_process_init() == pdFAIL)
        return CMD_PROCESS_INIT_ERR;

    // this thread will controll the created & deleted thread of tcp ip that dinamically
    if (create_supervision_thd(this_thread_id, tcp_client_thread, listen_port_thd) == pdFAIL)
        return CREATE_SPV_THD_ERR;

    own_cfg = get_config_ptr();

    /* Block to wait for prvTask1() to notify this task. */
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

    for (u8_t i = 0; i < MAX_TCP_PORT; i++)
    {
        // minus indicated not set
        if (own_cfg->listen_port[i] >= 0)
        {
            if (createServer(&new_obj_port, (u16_t)own_cfg->listen_port[i]) != pdPASS)
                return CREATE_SERVER_OBJ_ERR;

            if (CreateListenPortThd(new_obj_port) != pdPASS)
                return CREATE_LPORT_THD_ERR;
        }
    }

    lPortThdBit = xEventGroupGetBits(lPortThdEvGroup);

#ifdef DEBUG_ME
    printf("Success [0x%x] %d createServer listen port\r\n",
           lPortThdBit,
           count_num_of_flag_set(lPortThdBit));
#endif

    if (lPortThdBit == 0)
        return LPORT_EVENT_BIT_ERR;

    return SUCCESS;
}

#endif /* LWIP_NETCONN */
