#include "can_driver.h"
#include "servo_motor.h"
#include "stm32h7xx_hal_fdcan.h"

extern FDCAN_HandleTypeDef hfdcan2;

FDCAN_RxHeaderTypeDef RxHeader;
FDCAN_TxHeaderTypeDef TxHeader;

uint8_t TxData[8] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
uint8_t RxData[8];

uint32_t TxMailbox;
osMessageQueueId_t qCAN_Rx_IT;

u8_t send_CAN(u8_t *buff, u8_t len);

void CAN_thread(void *arg)
{
    osStatus_t qStatus;

    Servo_CMD_Init(&TxHeader);

    printf("%s\r\n", __func__);

    while (1)
    {
        osDelay(1);

        if (send_CAN(TxData, FDCAN_DLC_BYTES_8) != pdPASS)
            printf("Err: Failed Send_CAN!\r\n");

#if 1
        qStatus = osMessageQueueGet(qCAN_Rx_IT, (void *)&RxHeader, NULL, 1);
        if (qStatus == osOK)
        {
            /*TODO: process CAN RX here*/
            printf("There is data CAN received\r\n");

            printf("Identifier = %d\r\n", RxHeader.Identifier);
            printf("IdType = %d\r\n", RxHeader.IdType);
            printf("DataLength = %d\r\n", RxHeader.DataLength);
            printf("Recv:\r\n");
            for (int a = 0; a < sizeof(RxData); a++)
            {
                printf("0x%x \r\n", RxData[a]);
            }
        }
        else
        {
            printf("Warn: No data CAN received [%d] !\r\n", qStatus);
        }
#endif
    }
}

void init_CAN_thread(void *arg)
{
    printf("%s\r\n", __func__);

#define LEN_RX_QUEUE 10
    qCAN_Rx_IT = osMessageQueueNew(LEN_RX_QUEUE, sizeof(void *), NULL);

    osThreadNew(CAN_thread, NULL, NULL);
    osThreadExit();
}

u8_t send_CAN(u8_t *buff, u8_t len)
{
    if (IS_FDCAN_DLC(len) != pdTRUE)
        return pdFAIL;

    if (HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan2, &TxHeader, buff) != HAL_OK)
        return pdFAIL;

    return pdPASS;
}

void HAL_FDCAN_RxFifo0Callback(FDCAN_HandleTypeDef *hfdcan, uint32_t RxFifo0ITs)
{
    if ((RxFifo0ITs & FDCAN_IT_RX_FIFO0_NEW_MESSAGE) != RESET)
    {
        if (HAL_FDCAN_GetRxMessage(hfdcan, FDCAN_RX_FIFO0, &RxHeader, RxData) != HAL_OK)
            Error_Handler();

        osMessageQueuePut(qCAN_Rx_IT, (void *)&RxHeader, 0, 0);

        if (HAL_FDCAN_ActivateNotification(hfdcan, FDCAN_IT_RX_FIFO0_NEW_MESSAGE, 0) != HAL_OK)
            Error_Handler();
    }
}
