#include "pelco_d_protocol.h"

// HOSODO HSD-VC200 USB HD PTZ VIDEO CONFERENCING
// just support standard command

extern UART_HandleTypeDef huart2;

u8_t check_sum_calc(u8_t *buff, u8_t len)
{
    int sum_of_byte = 0;
    for (u8_t a = 1; a <= len - 2; a++)
    {
        sum_of_byte += buff[a];
    }
    return sum_of_byte % 100;
}

#define SET_ZERO_POSITION 0x0049

#define SET_PAN_POSITION 0x004B
#define QUERY_PAN_POSITION 0x0051
#define QUERY_PAN_POSITION_RESPONSE 0x0059

u8_t Set_Pan_Position(u16_t pan)
{
    pelco_d_data_t data_pelco_d = {.Sync = SYNC_BYTE,
                                   .Camera_Address = ADDRESS_CAMERA,
                                   .Command_1 = (SET_PAN_POSITION & 0xFF00) >> 8,
                                   .Command_2 = SET_PAN_POSITION & 0x00FF,
                                   .Data_1 = (pan & 0xFF00) >> 8,
                                   .Data_2 = pan & 0x00FF,
                                   .Checksum = 0};

    data_pelco_d.Checksum = check_sum_calc((u8_t *)&data_pelco_d, sizeof(data_pelco_d));
    HAL_UART_Transmit(&huart2, (u8_t *)&data_pelco_d, sizeof(data_pelco_d), 1000);

    return pdPASS;
}

u8_t Get_Pan_Position(u16_t *pan)
{
    pelco_d_data_t data_pelco_d = {.Sync = SYNC_BYTE,
                                   .Camera_Address = ADDRESS_CAMERA,
                                   .Command_1 = (QUERY_PAN_POSITION & 0xFF00) >> 8,
                                   .Command_2 = QUERY_PAN_POSITION & 0x00FF,
                                   .Data_1 = 0,
                                   .Data_2 = 0,
                                   .Checksum = 0};

    data_pelco_d.Checksum = check_sum_calc((u8_t *)&data_pelco_d, sizeof(data_pelco_d));
    if (HAL_UART_Transmit(&huart2, (u8_t *)&data_pelco_d, sizeof(data_pelco_d), 1000) > 0)
    {
        return pdFAIL;
    }
    printf("HAL_UART_Transmit\r\n");

    if (HAL_UART_Receive(&huart2, (u8_t *)&data_pelco_d, sizeof(data_pelco_d), 1000) > 0)
    {
        printf("HAL_UART_Receive failed\r\n");
        return pdFAIL;
    }
    // The position is given in hundredths of a degree and has a range from 0 ?�� 35999 (decimal)
    // Example: a position value of 4500 indicates 45 degrees.
    *pan = (data_pelco_d.Data_1 << 8) | data_pelco_d.Data_2;
    printf("pan position %d\r\n", *pan);
    return pdPASS;
}

u8_t Set_Zero_Position(void)
{
    pelco_d_data_t data_pelco_d = {.Sync = SYNC_BYTE,
                                   .Camera_Address = ADDRESS_CAMERA,
                                   .Command_1 = (SET_ZERO_POSITION & 0xFF00) >> 8,
                                   .Command_2 = SET_ZERO_POSITION & 0x00FF,
                                   .Data_1 = 0,
                                   .Data_2 = 0,
                                   .Checksum = 0};

    data_pelco_d.Checksum = check_sum_calc((u8_t *)&data_pelco_d, sizeof(data_pelco_d));
    HAL_UART_Transmit(&huart2, (u8_t *)&data_pelco_d, sizeof(data_pelco_d), 1000);

    return pdPASS;
}

// Pan / Tilt / Zoom / Iris etc.
u8_t stop_all_action(void)
{
    union command_1 cmd_1;
    union command_2 cmd_2;
    cmd_1.byte = 0;
    cmd_2.byte = 0;

    pelco_d_data_t data_pelco_d = {.Sync = SYNC_BYTE,
                                   .Camera_Address = ADDRESS_CAMERA,
                                   .Command_1 = cmd_1.byte,
                                   .Command_2 = cmd_2.byte,
                                   .Data_1 = 0,
                                   .Data_2 = 0,
                                   .Checksum = 0};

    data_pelco_d.Checksum = check_sum_calc((u8_t *)&data_pelco_d, sizeof(data_pelco_d));
    HAL_UART_Transmit(&huart2, (u8_t *)&data_pelco_d, sizeof(data_pelco_d), 1000);

    return pdPASS;
}

u8_t pan_move(pan_t move, u8_t speed)
{
    union command_1 cmd_1;
    union command_2 cmd_2;
    cmd_1.byte = 0;
    cmd_2.byte = 0;

    if (move == RIGHT_PAN)
        cmd_2.bit.Pan_Right = 1;
    else if (move == LEFT_PAN)
        cmd_2.bit.Pan_Left = 1;
    else if (move == STOP_PAN)
        __NOP;
    else
        return pdFAIL;

    if (move == STOP_PAN && speed > 0)
        return pdFAIL;

    // 0xff turbo max
    // 0x3f high speed
    // 0x20 medium speed
    if (speed > 0x3f)
        speed = 0x3f;

    pelco_d_data_t data_pelco_d = {.Sync = SYNC_BYTE,
                                   .Camera_Address = ADDRESS_CAMERA,
                                   .Command_1 = cmd_1.byte,
                                   .Command_2 = cmd_2.byte,
                                   .Data_1 = speed,
                                   .Data_2 = 0,
                                   .Checksum = 0};

    data_pelco_d.Checksum = check_sum_calc((u8_t *)&data_pelco_d, sizeof(data_pelco_d));
    HAL_UART_Transmit(&huart2, (u8_t *)&data_pelco_d, sizeof(data_pelco_d), 1000);

    return pdPASS;
}

u8_t tilt_move(tilt_t move, u8_t speed)
{
    union command_1 cmd_1;
    union command_2 cmd_2;
    cmd_1.byte = 0;
    cmd_2.byte = 0;

    if (move == UP_TILT)
        cmd_2.bit.Tilt_Up = 1;
    else if (move == DOWN_TILT)
        cmd_2.bit.Tilt_Down = 1;
    else if (move == STOP_TILT)
        __NOP();
    else
        return pdFAIL;

    if (move == STOP_TILT && speed > 0)
        return pdFAIL;

    // 0x3f high speed
    // 0x20 medium speed
    if (speed > 0x3f)
        speed = 0x3f;

    pelco_d_data_t data_pelco_d = {.Sync = SYNC_BYTE,
                                   .Camera_Address = ADDRESS_CAMERA,
                                   .Command_1 = cmd_1.byte,
                                   .Command_2 = cmd_2.byte,
                                   .Data_1 = 0,
                                   .Data_2 = speed,
                                   .Checksum = 0};

    data_pelco_d.Checksum = check_sum_calc((u8_t *)&data_pelco_d, sizeof(data_pelco_d));
    HAL_UART_Transmit(&huart2, (u8_t *)&data_pelco_d, sizeof(data_pelco_d), 1000);

    return pdPASS;
}
