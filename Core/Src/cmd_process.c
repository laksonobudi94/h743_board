#include "cmd_process.h"
#include "flash_data.h"
#include <string.h>
#include "tcpecho.h"
#include "lwip.h"
#include "lwip/init.h"
#include "lwip/netif.h"
#include "evpu_protocol.h"
#include "lwip/tcp.h"

extern struct netif gnetif;
id_client_t identity_client[MAX_TCP_CLIENT] = {0};
QueueHandle_t qCmdProcess;
QueueHandle_t qReplyCmdProcessClient[MAX_TCP_CLIENT];

u8_t one_time = 0;

static u8_t processBuff[2048];

typedef struct
{
    u32_t address_data;
    u32_t len_data;
    u32_t block;
    u32_t address_ptr;
} lvl_t;

void free_queue(QueueHandle_t queue_free)
{
    xQueueReset(queue_free);
    vQueueDelete(queue_free);
}

u8_t get_idx_queue_reply(u32_t addr, u8_t *val)
{
    for (u8_t i = 0; i < MAX_TCP_CLIENT; i++)
    {
        if (identity_client[i].addr == addr)
        {
            *val = i;
            return pdPASS;
        }
    }
    return pdFAIL; // there is not same
}

u8_t clear_identity_client(u32_t addr)
{
    u8_t idx = 0;
    if (get_idx_queue_reply(addr, &idx) == pdPASS)
    {
        memset(&identity_client[idx], 0, sizeof(identity_client[idx]));
        return pdPASS;
    }
    else
        return pdFAIL;
}

u8_t set_identity_client(u32_t addr, u8_t idx)
{
    if (idx < MAX_TCP_CLIENT)
    {
        identity_client[idx].addr = addr;
        return pdPASS;
    }
    else
        return pdFAIL;
}

u8_t parse_buff(u8_t *buff, u16_t len, u8_t idx)
{
    struct netconn *tcp_con_now;
    struct tcp_pcb *tcp;
    cfg_dat_t *own_cfg;
    ip_addr_t ip, netmask, gw;
    u16_t source_port;

    tcp_con_now = get_object_lwip(idx);
    tcp = tcp_con_now->pcb.tcp;
    tcp_tcp_get_tcp_addrinfo(tcp, 1, &ip, &source_port);

    if (buff[0] != FRAME_HEADER)
        return pdFALSE;

    if (buff[1] == GET_COMMAND)
    {
        switch (buff[2])
        {
        case IP_ADDR:
            memset(buff, 0, SIZE_BUFFER);
            sprintf((char *)buff, "Get STM32 IP address is %s\n", ipaddr_ntoa(&ip));
            break;
        case GATEWAY_ADDR:
            memset(buff, 0, SIZE_BUFFER);
            sprintf((char *)buff, "Get STM32 GATEWAY address is %x\n", ipaddr_ntoa(&gnetif.gw));
            break;
        case NETMASK_ADDR:
            memset(buff, 0, SIZE_BUFFER);
            sprintf((char *)buff, "Get STM32 NETMASK address is %s\n", ipaddr_ntoa(&gnetif.netmask));
            break;
        case LISTEN_PORT_1:
        case LISTEN_PORT_2:
        case LISTEN_PORT_3:
        case LISTEN_PORT_4:
        case LISTEN_PORT_5:
            memset(buff, 0, SIZE_BUFFER);
            sprintf((char *)buff, "Get STM32 listen port %d is %d\n", buff[2], own_cfg->listen_port[buff[2] - LISTEN_PORT_1]);
            break;
        default:
            return pdFALSE;
        }
    }
    else if (buff[1] == SET_COMMAND)
    {

        own_cfg = get_config_ptr();

        switch (buff[2])
        {
        case IP_ADDR:
            IP4_ADDR(&ip, buff[3], buff[4], buff[5], buff[6]);

            own_cfg->ip_address_0 = ip.addr;
            if (save_config() != pdPASS)
            {
                return pdFALSE;
            }

            memset(buff, 0, SIZE_BUFFER);
            sprintf((char *)buff, "Set STM32 IP address is %s\n", ipaddr_ntoa(&ip));
            break;
        case GATEWAY_ADDR:
            IP4_ADDR(&gw, buff[3], buff[4], buff[5], buff[6]);

            own_cfg->gateway = gw.addr;
            if (save_config() != pdPASS)
            {
                return pdFALSE;
            }

            memset(buff, 0, SIZE_BUFFER);
            sprintf((char *)buff, "Set STM32 GATEWAY address is %s\n", ipaddr_ntoa(&gw));
            break;
        case NETMASK_ADDR:
            IP4_ADDR(&netmask, buff[3], buff[4], buff[5], buff[6]);

            own_cfg->netmask = netmask.addr;
            if (save_config() != pdPASS)
            {
                return pdFALSE;
            }

            memset(buff, 0, SIZE_BUFFER);
            sprintf((char *)buff, "Set STM32 NETMASK address is %s\n", ipaddr_ntoa(&netmask));
            break;
        case LISTEN_PORT_1:
        case LISTEN_PORT_2:
        case LISTEN_PORT_3:
        case LISTEN_PORT_4:
        case LISTEN_PORT_5:
            own_cfg->listen_port[buff[2] - LISTEN_PORT_1] = buff[3];

            if (save_config() != pdPASS)
            {
                return pdFALSE;
            }

            memset(buff, 0, SIZE_BUFFER);
            sprintf((char *)buff, "Set STM32 listen port %d is %d\n", buff[2], own_cfg->listen_port[buff[2] - LISTEN_PORT_1]);
            break;
        default:
            return pdFALSE;
        }
    }
    else
        return pdFALSE;

    return pdTRUE;
}

err_enum_t DoCmdProcess(QueueHandle_t thd_queue,
                        u32_t source_port,
                        u8_t *buf,
                        u16_t *length,
                        u8_t *need_reply)
{
    Message send_msg, recv_msg;

    send_msg.payload_0 = CMD_THD;
    send_msg.payload_1 = source_port;
    send_msg.payload_2 = (u32_t)buf;
    send_msg.payload_3 = *length;

    if (xQueueSend(qCmdProcess, (void *)&send_msg, IMMEDIATELY) != pdTRUE)
    {
        if (xQueueSend(qCmdProcess, (void *)&send_msg, QSEND_WAIT_MS) != pdTRUE)
        {
#ifdef DEBUG_ME
            printf("Err: xQueueSend qCmdProcess Failed!\r\n");
#endif
            return ERR_MEM;
        }
    }

    if (xQueueReceive(thd_queue, (void *)&recv_msg, QRECV_TIMEOUT_MS) != pdTRUE)
        return ERR_MEM;

    if (recv_msg.payload_0 == CMD_THD && recv_msg.payload_4 == ERR_OK)
    {
        memcpy(buf, (u8_t *)recv_msg.payload_1, recv_msg.payload_2);
        *length = recv_msg.payload_2;
        *need_reply = recv_msg.payload_3;
    }
    else
        return ERR_MEM;

    return ERR_OK;
}

static void cmd_process_thread(void *arg)
{
    Message recv_msg, send_msg;
    u16_t len;
    u8_t idx;
    u32_t source_port;
    struct netconn *tcp_con_now;
    struct tcp_pcb *tcp;
    u16_t listen_port;
    u8_t reply;
    void *data;

    while (1)
    {
        osDelay(1);
        if (xQueueReceive(qCmdProcess, (void *)&recv_msg, osWaitForever) != pdTRUE)
            continue;

        if (recv_msg.payload_0 != CMD_THD)
            continue;

        source_port = recv_msg.payload_1;
        data = (void *)recv_msg.payload_2;
        len = recv_msg.payload_3;

#ifdef DEBUG_ME
        printf("From source port TCP = %d\r\n", source_port);
#endif

        // reply based on source port
        if (get_idx_queue_reply(source_port, &idx) != pdTRUE)
        {
#ifdef DEBUG_ME
            printf("Error: No found source port!!!\r\n");
#endif
            continue;
        }

        // reply based on listen port
        tcp_con_now = get_object_lwip(idx);
        tcp = tcp_con_now->pcb.tcp;
        tcp_tcp_get_tcp_addrinfo(tcp, 1, NULL, &listen_port);

        // must copy dynamic mem from lwip then destroy
        memset(&processBuff[0], 0, sizeof(processBuff));
        memcpy((void *)&processBuff[0], (void *)data, len);
#if 0
        printf("Recv TCP -> ");
        for (u16_t a = 0; a < len; a++)
            printf("%02x ", *(processBuff + a));
        printf("\r\n");
#endif

        if (listen_port == EVPU_PROTOCOL_PORT)
        {
            if (!one_time)
            {
                set_obj_evpu_tcp(tcp_con_now);
                one_time = 1;
                /*TODO : When disconnect evpu must be free object*/
            }
            if (evpu_parse_buff(processBuff, &len, &reply) != pdPASS)
            {
                printf("Err: evpu_parse_buff Failed!\r\n");
            }
        }
        else
        {
            parse_buff(processBuff, len, idx);
            len = strlen((char *)processBuff);
        }

        send_msg.payload_0 = (u32_t)CMD_THD;
        send_msg.payload_1 = (u32_t)&processBuff[0];
        send_msg.payload_2 = (u32_t)len;
        send_msg.payload_3 = (u32_t)reply;
        send_msg.payload_4 = (u32_t)ERR_OK;

        if (xQueueSend(get_queue_reply(idx), (void *)&send_msg, IMMEDIATELY) != pdTRUE)
        {
            if (xQueueSend(get_queue_reply(idx), (void *)&send_msg, QSEND_WAIT_MS) != pdTRUE)
            {
#ifdef DEBUG_ME
                printf("Error: xQueueSend qReplyCmdProcessClient Failed!!!\r\n");
#endif
            }
        }
    }
}

QueueHandle_t create_queue_reply(u8_t num_queue)
{
    qReplyCmdProcessClient[num_queue] =
        xQueueCreate(REPLY_qCmdProcess_LEN, sizeof(Message));

    return qReplyCmdProcessClient[num_queue];
}

QueueHandle_t get_queue_reply(u8_t num_queue)
{
    return qReplyCmdProcessClient[num_queue];
}

u8_t cmd_process_init(void)
{
    qCmdProcess = xQueueCreate(qCmdProcess_LEN, sizeof(Message));
    if (qCmdProcess == NULL)
        return pdFAIL;

    if (sys_thread_new("cmd_process_thd",
                       cmd_process_thread,
                       NULL,
                       STACK_SIZE_CMD,
                       osPriorityNormal) == NULL)
    {
        return pdFAIL;
    }

    return pdPASS;
}
