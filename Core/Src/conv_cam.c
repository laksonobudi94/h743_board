#include "conv_cam.h"
#include "evpu_protocol.h"
#include <string.h>

s32_t map_s16_to_s32(s16_t x, s16_t in_min, s16_t in_max, s32_t out_min, s32_t out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

s16_t map_s16(s16_t x, s16_t in_min, s16_t in_max, s16_t out_min, s16_t out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

s16_t map_s32_to_s16(s32_t x, s32_t in_min, s32_t in_max, s16_t out_min, s16_t out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

size_t CMD_Pan_Tilt_Drive(Pan_tiltDrive_t move_type, u8_t *buff, u8_t Pan_speed, u8_t Tilt_speed)
{
    size_t len = 0;
    if (buff == NULL)
        return pdFAIL;

    if (Pan_speed < MIN_LOW_SPEED)
        Pan_speed = MIN_LOW_SPEED;
    if (Tilt_speed < MIN_LOW_SPEED)
        Tilt_speed = MIN_LOW_SPEED;

    if (Pan_speed > MAX_HIGH_SPEED)
        Pan_speed = MAX_HIGH_SPEED;
    if (Tilt_speed > MAX_HIGH_SPEED)
        Tilt_speed = MAX_HIGH_SPEED;

    switch (move_type)
    {
    case Up:
        len = sizeof(Pan_tiltDrive_Up);
        memcpy(buff, Pan_tiltDrive_Up, len);
        break;
    case Down:
        len = sizeof(Pan_tiltDrive_Down);
        memcpy(buff, Pan_tiltDrive_Down, len);
        break;
    case Left:
        len = sizeof(Pan_tiltDrive_Left);
        memcpy(buff, Pan_tiltDrive_Left, len);
        break;
    case Right:
        len = sizeof(Pan_tiltDrive_Right);
        memcpy(buff, Pan_tiltDrive_Right, len);
        break;
    case Upleft:
        len = sizeof(Pan_tiltDrive_UpLeft);
        memcpy(buff, Pan_tiltDrive_UpLeft, len);
        break;
    case Upright:
        len = sizeof(Pan_tiltDrive_UpRight);
        memcpy(buff, Pan_tiltDrive_UpRight, len);
        break;
    case Downleft:
        len = sizeof(Pan_tiltDrive_DownLeft);
        memcpy(buff, Pan_tiltDrive_DownLeft, len);
        break;
    case Downright:
        len = sizeof(Pan_tiltDrive_DownRight);
        memcpy(buff, Pan_tiltDrive_DownRight, len);
        break;
    case Stop:
        len = sizeof(Pan_tiltDrive_Stop);
        memcpy(buff, Pan_tiltDrive_Stop, len);
        break;
    case Home:
        len = sizeof(Pan_tiltDrive_Home);
        memcpy(buff, Pan_tiltDrive_Home, len);
        break;
    case Reset:
        len = sizeof(Pan_tiltDrive_Reset);
        memcpy(buff, Pan_tiltDrive_Reset, len);
        break;

    default:
        return pdFAIL;
    }

    if (move_type == Home || move_type == Reset)
        return len;

    buff[4] = Pan_speed;
    buff[5] = Tilt_speed;

    return len;
}

u8_t Parse_Get_PT_Position(u8_t *buff, u16_t *pan_visca_value, u16_t *tilt_visca_value)
{
    if (buff == NULL | pan_visca_value == NULL | tilt_visca_value == NULL)
        return pdFAIL;

    *pan_visca_value = (*(buff + 2) & 0x0F) << 12 |
                       (*(buff + 3) & 0x0F) << 8 |
                       (*(buff + 4) & 0x0F) << 4 |
                       (*(buff + 5) & 0x0F) << 0;

    *tilt_visca_value = (*(buff + 6) & 0x0F) << 12 |
                        (*(buff + 7) & 0x0F) << 8 |
                        (*(buff + 8) & 0x0F) << 4 |
                        (*(buff + 9) & 0x0F) << 0;

    return pdPASS;
}

u16_t CMD_Get_PT_Position(u8_t *buff)
{
    u16_t len = sizeof(Pan_tiltPosInq);
    memcpy(buff, Pan_tiltPosInq, len);

    return len;
}

u8_t Parse_Get_PT_Speed(u8_t *Pan_speed, u8_t *Tilt_speed)
{
    return pdPASS;
}

u8_t CMD_Set_PT_Position(Pan_tiltDrive_t move_type, u16_t Pan_Position, u16_t Tilt_Position, u8_t Pan_speed, u8_t Tilt_speed, u8_t *buff)
{
    u16_t len = 0;

    // printf("%s\r\n", __func__);

    switch (move_type)
    {
    case AbsolutePosition:
        len = sizeof(Pan_tiltDrive_AbsolutePosition);
        memcpy(buff, Pan_tiltDrive_AbsolutePosition, len);
        break;
    case RelativePosition:
        len = sizeof(Pan_tiltDrive_RelativePosition);
        memcpy(buff, Pan_tiltDrive_RelativePosition, len);
        break;
    default:
        return 0;
    }

    buff[4] = Pan_speed;
    buff[5] = Tilt_speed;

    buff[6] = (u8_t)((Pan_Position & 0xF000) >> 12);
    buff[7] = (u8_t)((Pan_Position & 0x0F00) >> 8);
    buff[8] = (u8_t)((Pan_Position & 0x00F0) >> 4);
    buff[9] = (u8_t)((Pan_Position & 0x000F));

    buff[10] = (u8_t)((Tilt_Position & 0xF000) >> 12);
    buff[11] = (u8_t)((Tilt_Position & 0x0F00) >> 8);
    buff[12] = (u8_t)((Tilt_Position & 0x00F0) >> 4);
    buff[13] = (u8_t)((Tilt_Position & 0x000F));

    return len;
}

err_t check_position(s16_t velo_val,
                     u16_t limit,
                     u16_t pos_now,
                     s16_t *temp_velo,
                     u16_t *set_pos,
                     s16_t *move_elevate)
{
    if (velo_val >= limit)
        velo_val = limit;
    if (velo_val <= limit * -1)
        velo_val = limit * -1;

    if (velo_val != 0 && *temp_velo != 0)
    {
        // we not accept new move when value is same
        if (velo_val == *temp_velo)
            return ERR_ABRT;
    }
    *move_elevate = velo_val;
    *temp_velo = velo_val;

    // velo is pos now + value
    *set_pos = pos_now + velo_val;

    return ERR_OK;
}

u8_t set_home_PT(void)
{
    u16_t len;
    u8_t ownBuf[64];

    memset(ownBuf, 0, sizeof(ownBuf));

    len = CMD_Pan_Tilt_Drive(Home, ownBuf, MAX_HIGH_SPEED, MAX_HIGH_SPEED);

    if (len == 0)
        return pdFAIL;

    if (CMD_TransReceive(ownBuf, &len, SEM_TIMEOUT) != pdPASS)
        return pdFAIL;

    if (len == 0)
        return pdFAIL;

    return pdPASS;
}

u8_t get_encoder_pos(u16_t *azimuth_encoder_pos,
                     u16_t *elevace_encoder_pos,
                     s16_t *pan_angle,
                     s16_t *tilt_angle)
{
    u16_t len, pan_visca_pos, tilt_visca_pos;
    cfg_t *pCfg;
    u8_t ownBuf[64];

    memset(ownBuf, 0, sizeof(ownBuf));
    pCfg = get_cfg_mainboard();
    len = CMD_Get_PT_Position(ownBuf);
    if (len == 0)
        return pdFAIL;

    if (CMD_TransReceive(ownBuf, &len, SEM_TIMEOUT) != pdPASS)
        return pdFAIL;

    if (len == 0)
        return pdFAIL;

    if (Parse_Get_PT_Position(ownBuf, &pan_visca_pos, &tilt_visca_pos) != pdPASS)
        return pdFAIL;

    if (pan_angle != NULL)
    {
        *pan_angle = map_s16((s16_t)pan_visca_pos,
                             MINUS_170_DEGREE,
                             PLUS_170_DEGREE,
                             -170,
                             170);
    }

    if (tilt_angle != NULL)
    {
        *tilt_angle = map_s16((s16_t)tilt_visca_pos,
                              MINUS_30_DEGREE,
                              PLUS_90_DEGREE,
                              -30,
                              90);
    }

    if (azimuth_encoder_pos != NULL)
    {
        *azimuth_encoder_pos = (u16_t)map_s16_to_s32((s16_t)pan_visca_pos,
                                                     MINUS_170_DEGREE,
                                                     PLUS_170_DEGREE,
                                                     0,
                                                     65535);
    }

    if (elevace_encoder_pos != NULL)
    {
        *elevace_encoder_pos = (u16_t)map_s16_to_s32((s16_t)tilt_visca_pos,
                                                     MINUS_30_DEGREE,
                                                     PLUS_90_DEGREE,
                                                     pCfg->min_elev_deg,
                                                     pCfg->max_elev_deg);
    }

    return pdPASS;
}
