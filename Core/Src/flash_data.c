#include "flash_data.h"
#include "string.h"

#include "stm32_hal_legacy.h"

static cfg_dat_t this_cfg = {0};

void addr_to_buff(u32_t addr, u8_t *buff)
{
    buff[0] = (u8_t)((addr & 0xFF000000) >> 24);
    buff[1] = (u8_t)((addr & 0x00FF0000) >> 16);
    buff[2] = (u8_t)((addr & 0x0000FF00) >> 8);
    buff[3] = (u8_t)((addr & 0x000000FF));
}

static void default_config(cfg_dat_t *def_cfg)
{
    ip4_addr_t ipaddr;
    ip4_addr_t netmask;
    ip4_addr_t gw;

    u8_t IP_ADDRESS[4] = {192, 168, 0, 100};
    u8_t NETMASK_ADDRESS[4] = {255, 255, 255, 0};
    u8_t GATEWAY_ADDRESS[4] = {192, 168, 0, 1};

    IP4_ADDR(&ipaddr, IP_ADDRESS[3], IP_ADDRESS[2], IP_ADDRESS[1], IP_ADDRESS[0]);
    IP4_ADDR(&netmask, NETMASK_ADDRESS[3], NETMASK_ADDRESS[2], NETMASK_ADDRESS[1], NETMASK_ADDRESS[0]);
    IP4_ADDR(&gw, GATEWAY_ADDRESS[3], GATEWAY_ADDRESS[2], GATEWAY_ADDRESS[1], GATEWAY_ADDRESS[0]);

    def_cfg->ip_address_0 = ipaddr.addr;
    def_cfg->ip_address_1 = ipaddr.addr;
    def_cfg->netmask = netmask.addr;
    def_cfg->gateway = gw.addr;

    def_cfg->listen_port[0] = 501;   // onviv protocol prepared
    def_cfg->listen_port[1] = 10000; // evpu protocol
}

u8_t config_init(void)
{
    u8_t force_default = 1;
    u8_t local_buff[sizeof(this_cfg)];
    memset(local_buff, 0xff, sizeof(this_cfg));
    read_cfg_from_mem((u8_t *)&this_cfg, sizeof(this_cfg));

    if (memcmp(local_buff, (u8_t *)&this_cfg, sizeof(this_cfg)) == 0 || force_default)
    {
        default_config(&this_cfg);
        printf("WARNING: No Content of Cfg in Flash!!!\r\n");
        printf("WARNING: Make to default Cfg!!!\r\n");
        // return save_config();
    }

    return pdPASS;
}

cfg_dat_t *get_config_ptr(void)
{
    return &this_cfg;
}

u8_t save_config(void)
{
    if (save_cfg_from_mem((u8_t *)&this_cfg, sizeof(this_cfg)) != pdPASS)
    {
        return pdFALSE;
    }

    return pdPASS;
}

static void erase_sector_cfg(void)
{
    HAL_FLASH_Unlock();
#if !defined(STM32H7)
    FLASH_Erase_Sector(SECTOR_CFG, FLASH_VOLTAGE_RANGE_3);
#else
    FLASH_Erase_Sector(SECTOR_CFG, BANK_CFG, FLASH_VOLTAGE_RANGE_3);

#endif
    HAL_FLASH_Lock();
}

static u8_t read_cfg_from_mem(u8_t *data, u32_t len)
{

    u32_t addr = ADDR_CFG;

    HAL_FLASH_Unlock();
    for (u16_t i = 0; i < len; i++)
    {
        data[i] = *(__IO uint8_t *)addr;
        addr++;
    }
    HAL_FLASH_Lock();

    return pdPASS;
}

static u8_t save_cfg_from_mem(u8_t *data, u32_t len)
{
    u32_t addr = ADDR_CFG;
	u16_t *data_word = (u16_t *)data;
    erase_sector_cfg();

    HAL_FLASH_Unlock();
#if !defined(STM32H7)
    for (u16_t i = 0; i < len; i++)
    {
        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, addr, data[i]) != HAL_OK)
            return pdFAIL;
    }
#else
    for (u16_t i = 0; i < len/2; i++)
    {
        if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_FLASHWORD, addr, (u16_t)data_word[i]) != HAL_OK)
            return pdFAIL;
    }
#endif
    HAL_FLASH_Lock();

    return pdPASS;
}
